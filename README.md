# Methods Course<br>Data Evaluation with Python: Tools and Good Practice

Nils Luettschwager, October 2022

This repository holds the material for the course "Data Evaluation with Python:
Tools and Good Practice". The course is divided in two parts. The first part is
about software development tools (e.g. version control software), the second
part focuses on specific Python libraries that are typically used in scientific
computing. Each part has several "chapters" and each chapter is accompanied by
at least one Jupyter notebook. The notebooks are written such that they can
also be worked through in self-study. If you find something unclear, the best
way to communicate the problem is to open an issue on
[https://gitlab.gwdg.de/jupyter-course/jupyter-course/issues](https://gitlab.gwdg.de/jupyter-course/jupyter-course/issues).
In addition to the chapters, I prepared an [example
project](https://gitlab.gwdg.de/jupyter-course/sample-project), which we will
look into on the first course day to get an idea of how a project build around
Python and Jupyter can look like. In [glossary.md](glossary.md) you find some
definitions of technical terms. We can amend this file during the course, if
required.

The entry-point for the course material is the file [index.md](index.md).

## Preparing for the course

You should login at [gitlab.gwdg.de](https://gitlab.gwdg.de) once to check that
it works (you have to use your email address as username).

You can try to follow the guidelines below to install the course material and
sample project on [jupyter-cloud.gwdg.de](https://jupyter-cloud.gwdg.de) (the
recommended way; login with your GWDG account) or your local machine (may be
more difficult). Otherwise, you can check out the material on binder (next
section). Installing the course material will be part of the course itself.

You can install the editor [Visual Studio Code](https://code.visualstudio.com/)
(optional).

## Running on binder

The easiest and recommended way to use the course material is to run it on
[mybinder.org](https://mybinder.org). No installation or dependency handling
required. This is how we will begin the course.

To start your binder session, click this badge:

[![badge](https://img.shields.io/badge/launch-binder-F5A252.svg?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFkAAABZCAMAAABi1XidAAAB8lBMVEX///9XmsrmZYH1olJXmsr1olJXmsrmZYH1olJXmsr1olJXmsrmZYH1olL1olJXmsr1olJXmsrmZYH1olL1olJXmsrmZYH1olJXmsr1olL1olJXmsrmZYH1olL1olJXmsrmZYH1olL1olL0nFf1olJXmsrmZYH1olJXmsq8dZb1olJXmsrmZYH1olJXmspXmspXmsr1olL1olJXmsrmZYH1olJXmsr1olL1olJXmsrmZYH1olL1olLeaIVXmsrmZYH1olL1olL1olJXmsrmZYH1olLna31Xmsr1olJXmsr1olJXmsrmZYH1olLqoVr1olJXmsr1olJXmsrmZYH1olL1olKkfaPobXvviGabgadXmsqThKuofKHmZ4Dobnr1olJXmsr1olJXmspXmsr1olJXmsrfZ4TuhWn1olL1olJXmsqBi7X1olJXmspZmslbmMhbmsdemsVfl8ZgmsNim8Jpk8F0m7R4m7F5nLB6jbh7jbiDirOEibOGnKaMhq+PnaCVg6qWg6qegKaff6WhnpKofKGtnomxeZy3noG6dZi+n3vCcpPDcpPGn3bLb4/Mb47UbIrVa4rYoGjdaIbeaIXhoWHmZYHobXvpcHjqdHXreHLroVrsfG/uhGnuh2bwj2Hxk17yl1vzmljzm1j0nlX1olL3AJXWAAAAbXRSTlMAEBAQHx8gICAuLjAwMDw9PUBAQEpQUFBXV1hgYGBkcHBwcXl8gICAgoiIkJCQlJicnJ2goKCmqK+wsLC4usDAwMjP0NDQ1NbW3Nzg4ODi5+3v8PDw8/T09PX29vb39/f5+fr7+/z8/Pz9/v7+zczCxgAABC5JREFUeAHN1ul3k0UUBvCb1CTVpmpaitAGSLSpSuKCLWpbTKNJFGlcSMAFF63iUmRccNG6gLbuxkXU66JAUef/9LSpmXnyLr3T5AO/rzl5zj137p136BISy44fKJXuGN/d19PUfYeO67Znqtf2KH33Id1psXoFdW30sPZ1sMvs2D060AHqws4FHeJojLZqnw53cmfvg+XR8mC0OEjuxrXEkX5ydeVJLVIlV0e10PXk5k7dYeHu7Cj1j+49uKg7uLU61tGLw1lq27ugQYlclHC4bgv7VQ+TAyj5Zc/UjsPvs1sd5cWryWObtvWT2EPa4rtnWW3JkpjggEpbOsPr7F7EyNewtpBIslA7p43HCsnwooXTEc3UmPmCNn5lrqTJxy6nRmcavGZVt/3Da2pD5NHvsOHJCrdc1G2r3DITpU7yic7w/7Rxnjc0kt5GC4djiv2Sz3Fb2iEZg41/ddsFDoyuYrIkmFehz0HR2thPgQqMyQYb2OtB0WxsZ3BeG3+wpRb1vzl2UYBog8FfGhttFKjtAclnZYrRo9ryG9uG/FZQU4AEg8ZE9LjGMzTmqKXPLnlWVnIlQQTvxJf8ip7VgjZjyVPrjw1te5otM7RmP7xm+sK2Gv9I8Gi++BRbEkR9EBw8zRUcKxwp73xkaLiqQb+kGduJTNHG72zcW9LoJgqQxpP3/Tj//c3yB0tqzaml05/+orHLksVO+95kX7/7qgJvnjlrfr2Ggsyx0eoy9uPzN5SPd86aXggOsEKW2Prz7du3VID3/tzs/sSRs2w7ovVHKtjrX2pd7ZMlTxAYfBAL9jiDwfLkq55Tm7ifhMlTGPyCAs7RFRhn47JnlcB9RM5T97ASuZXIcVNuUDIndpDbdsfrqsOppeXl5Y+XVKdjFCTh+zGaVuj0d9zy05PPK3QzBamxdwtTCrzyg/2Rvf2EstUjordGwa/kx9mSJLr8mLLtCW8HHGJc2R5hS219IiF6PnTusOqcMl57gm0Z8kanKMAQg0qSyuZfn7zItsbGyO9QlnxY0eCuD1XL2ys/MsrQhltE7Ug0uFOzufJFE2PxBo/YAx8XPPdDwWN0MrDRYIZF0mSMKCNHgaIVFoBbNoLJ7tEQDKxGF0kcLQimojCZopv0OkNOyWCCg9XMVAi7ARJzQdM2QUh0gmBozjc3Skg6dSBRqDGYSUOu66Zg+I2fNZs/M3/f/Grl/XnyF1Gw3VKCez0PN5IUfFLqvgUN4C0qNqYs5YhPL+aVZYDE4IpUk57oSFnJm4FyCqqOE0jhY2SMyLFoo56zyo6becOS5UVDdj7Vih0zp+tcMhwRpBeLyqtIjlJKAIZSbI8SGSF3k0pA3mR5tHuwPFoa7N7reoq2bqCsAk1HqCu5uvI1n6JuRXI+S1Mco54YmYTwcn6Aeic+kssXi8XpXC4V3t7/ADuTNKaQJdScAAAAAElFTkSuQmCC)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fjupyter-course%2Fjupyter-course/HEAD)

Note that launching binder may take a moment.

If you want to keep your modifications of the course material, read on.

## Dependencies

I assume you install and run the course material on GWDG's JupyterHub service,
[https://jupyter-cloud.gwdg.de](https://jupyter-cloud.gwdg.de), which comes
with basic dependencies pre-installed and runs JupyterLab (instead of Jupyter
Notebook). If you prefer your own computer, you have to install Python and
JupyterLab locally, e.g. by downloading Python from
https://www.python.org/downloads/ and [installing JupyterLab via
pip](https://jupyterlab.readthedocs.io/en/latest/getting_started/installation.html#installation).
You will further have to install:

- [Git](https://git-scm.com/downloads)
- [`ipywidgets` for JupyterLab 3](https://ipywidgets.readthedocs.io/en/stable/index.html)
- [`pyviz_comms` >= 2.2](https://pypi.org/project/pyviz-comms/)

## Downloading the course material

Clone the course material *via* git (or make your own fork first and clone
that, see [version-control.ipynb](version-control.ipynb#Forking-a-Repository)).

Type in the terminal (command line):

```bash
git clone https://gitlab.gwdg.de/jupyter-course/jupyter-course.git --depth=1
```

You can also [download the project as a zip
file](https://gitlab.gwdg.de/jupyter-course/jupyter-course/-/archive/master/jupyter-course-master.zip)
and extract it to the location of your choice (I'll assume the folder name
`jupyter-course`).

## Installing the environment

We will use a dedicated environment for the course, as we should for every
project. The course material includes a `requirements.txt` file which lists all
the Python packages and versions that we need to install to run the code of the
course. The installation process is layed out in the following. Again, the
commands are to be typed into the terminal (command line). Executing some of
these commands, especially `pip install -r requirements.txt` can take a while,
be patient ...

Update JupyterLab in your *global environment* and install `pyviz_comms` and
`jupyterlab_widgets`. In a new terminal, type:

```bash
pip install -U "jupyterlab~=3.4" "pyviz_comms~=2.2" "jupyterlab_widgets~=3.0" --user
```

If you want to use the git integration through `nbdime`, do (optional):

```bash
pip install nbdime --user
jupyter labextension install nbdime-jupyterlab
jupyter serverextension enable --py nbdime --user
```

If you want the automatic code formatting (`jupyterlab-code-formatter`), do (optional):

```bash
pip install black isort jupyterlab-code-formatter --user
```

The nbdime and jupyterlab-code-formatter plug-ins require a server restart to
work. Restart your Jupyter server by clicking *File* (in the menu) &rarr; *Hub
Control Panel* and then the button *Stop My Server* and (wait a moment) *Start
My Server*.

Make a dedicated Python environment inside the folder `jupyter-course`. In a
new terminal, type:

```bash
cd jupyter-course
python3 -m venv env
```

Activate the environment:

```bash
source env/bin/activate
```

(Note that `(env)` will now be displayed at the beginning of the command
prompt, indicating that the environment *env* is active.)

Update `pip` and install `wheel`, a tool that helps installing other packages:

```bash
pip install -U pip wheel
```

Install the required packages:

```bash
pip install -r requirements.txt
```

Register a new Python kernel with Jupyter:

```bash
python -m ipykernel install --name jupyter-course --user
```

You should now be all set to use the course material. Enjoy!

---

## Uninstalling the environment

You can remove the course's environment as follows:

Delete the `env` folder inside the course material directory:

```bash
rm -r env
```

This takes quite long, because MANY small files have to be deleted. You can see
which files are deleted if you add the `-v` flag to the `rm` command:

```bash
rm -rv env
```

Uninstall the Jupyter kernel:

```bash
jupyter kernelspec remove jupyter-course
```

## Running from Docker (locally)

The Docker image defined in `Dockerfile_local` can be build via:

```bash
docker buildx build . -f Dockerfile_local --tag jupyter-course
```

and run by:

```bash
docker run --rm -it -p 8888:8888 --volume $(pwd):/home/jovyan/work jupyter-course
```

Note that the file `binder/Dockerfile` is included to run the repository on binder
and is not recommended for working locally, because changes to the files will
not be persistent if the container is removed.
