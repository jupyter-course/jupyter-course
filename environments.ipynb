{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Environments\n",
    "\n",
    "2021-03-17\n",
    "\n",
    "N. Luettschwager"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"toc\">*Table of contents*</a>\n",
    "\n",
    "- [Introduction](#Introduction)\n",
    "- [When things go wrong ...](#When-things-go-wrong-...)\n",
    "- [Different results](#Different-results)\n",
    "- [Creating a dedicated environment](#Creating-a-dedicated-environment)\n",
    "- [\"Freezing\" your environment](#\"Freezing\"-your-environment)\n",
    "- [Registering an environment with Jupyter](#Registering-an-environment-with-Jupyter)\n",
    "- [Removing an environment](#Removing-an-environment)\n",
    "- [Exercise](#Exercise)\n",
    "- [Project creation workflow](#Project-creation-workflow)\n",
    "- [Version numbers](#Version-numbers)\n",
    "- [Looking Beyond ...](#Looking-Beyond-...)\n",
    "\n",
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an anecdote that perhaps many new Python programmers can tell:\n",
    "\n",
    "> \"I once had this nice little program that I wanted to show to a colleague. Some work that I had done a couple of months ago. I fired up my terminal, ran the script and ... suprise, suprise: it *crashed* on me! It turned out that I updated my packages since I finished that project and one of the updates broke my code.\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the strengths of Python is its huge library of open source packages that are at your disposal, just a simple `pip install` away. This is really helpful and can save you a lot of time. You have to keep in mind though, that these **software packages** are (or should be) **actively maintained**, that is, they are living things that change over time.\n",
    "It is not always practical for the developers to keep **backwards compatibility** and sometimes, of course, an **update can introduce a bug**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## When things go wrong ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the example in the next cell. Let's assume you were using Matplotlib 2.2.4 (a previous long term support version). The code would work perfectly well and produce the stacked area plot you want:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import matplotlib as mpl\n",
    "print(f\"matplotlib version {mpl.__version__}\")\n",
    "\n",
    "# the next line will raise an TypeError in Matplotlib v3.0.0\n",
    "plt.stackplot([1, 2, 3], [1, 1, 2], [1, 2, 4], [1, 2, 1], labels=['A', 'B', 'C'])\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's say Matplotlib 3.0.0 becomes available (and you really want that all new [&rarr;\"twilight\" colormaps](https://matplotlib.org/users/prev_whats_new/whats_new_3.0.html#cyclic-colormaps)) and you upgrade.\n",
    "\n",
    "After the upgrade, you will find that the code above no longer works. Not because there was a change of the API (these are usually communicated early through deprecation warnings), but because some [&rarr;bug was introduced in Matplotlib 3.0.0](https://github.com/matplotlib/matplotlib/issues/12405). Matplotlib 3.0.1 fixes the bug, so after updating once more, your code works again."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see this behavior yourself. If you downgrade you Matplotlib package first to 2.2.4\n",
    "\n",
    "> ```bash\n",
    "> cd path/to/jupyter-course\n",
    "> source env/bin/activate\n",
    "> pip install matplotlib==2.2.4\n",
    "> ```\n",
    "\n",
    "... and you run the code, everything is fine. You install version 3.0.0\n",
    "\n",
    "> ```bash\n",
    "> pip install matplotlib==3.0.0\n",
    "> ```\n",
    "\n",
    "... and you get an error. Upgrading to 3.0.1 or the newest version (3.3.4 as of this writing)\n",
    "\n",
    "> ```bash\n",
    "> pip install matplotlib -U\n",
    "> ```\n",
    "\n",
    "and the code functions normally again."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Different results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Updates that break your code are easy to spot, but some updates may also introduce more **subtle changes** that you are not aware of. For example, you may re-run your code after an update and find that some results are slightly different. This can happen when package developers change the **particular implementation** of some algorithm. For example, the release notes of the 1.17 Numpy version state:\n",
    "\n",
    "> [&rarr; Replacement of the fftpack based fft module by the pocketfft library](https://numpy.org/doc/1.17/release.html#replacement-of-the-fftpack-based-fft-module-by-the-pocketfft-library)\n",
    ">\n",
    "> Both implementations have the same ancestor (Fortran77 FFTPACK by Paul N. Swarztrauber), but pocketfft contains additional modifications which improve both accuracy and performance in some circumstances. \\[...\\]\n",
    "\n",
    "Of course we want the improved accuracy, but we should be aware of it.\n",
    "Otherwise, we or other people may get confused why we/they cannot fully reproduce our results.\n",
    "\n",
    "If a package that you depend upon received an update, it may be a good idea to check the **release notes**, in particular if that package is used for some core computations, such as [Scipy](https://docs.scipy.org/doc/scipy/release.html) or [Numpy](https://numpy.org/doc/stable/release.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating a dedicated environment\n",
    "\n",
    "How can you protect yourself from bugs that are introduced through updates, incompatible API changes or updates that change your results in a subtle way? The solution to this problem is to create a dedicated, [&rarr; virtual environment](https://docs.python.org/3.10/library/venv.html#venv-def) for your project."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A virtual environment behaves **like a separate Python installation**. Packages that you install into a virtual environment are not visible to your base (system) environment, nor to any other environment. Your environment is not affected by updates of your operating system. A virtual environment can be created easily and it can be equipped with the required packages using a single command."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The main commands that you need to type into the terminal are as follows:\n",
    "\n",
    "1. `python3 -m venv path/to/your/environment`\n",
    "2. `source path/to/your/environment/bin/activate` (bash/Linux/macOS), `path/to/your/environment/Scripts/activate.bat` (Windows)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first command create the environment in the folder `path/to/your/environment` by running the module `venv` from the Python 3 standard library as a script (triggered by the `-m` flag). I will assume the folder name `env` in the following, which is a common choice. The second command activates the environment, meaning it prepends your [PATH variable](glossary.md#PATH-Variable) such that the commands `python` and `pip` (among others) now point to your new environment.\n",
    "After activating the environment, the `pip install` command will install packages into your new environment. Everything else remains untouched.\n",
    "\n",
    "You deactivate the environment with the command `deactivate` (you guessed it)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## \"Freezing\" your environment\n",
    "\n",
    "The dedicated environment in general and the tool `pip` in particular help us to define a very specific context in which our code should be executed.\n",
    "\n",
    "Try typing\n",
    "\n",
    "```bash\n",
    "pip freeze\n",
    "```\n",
    "\n",
    "into your terminal. For me this yielded the following list:\n",
    "\n",
    "```\n",
    "altair==4.2.0\n",
    "anyio==3.6.1\n",
    "argon2-cffi==21.3.0\n",
    "argon2-cffi-bindings==21.2.0\n",
    "astroid==2.12.11\n",
    "asttokens==2.0.8\n",
    "attrs==22.1.0\n",
    "Babel==2.10.3\n",
    "backcall==0.2.0\n",
    "beautifulsoup4==4.11.1\n",
    "bleach==5.0.1\n",
    "bokeh==2.4.3\n",
    "certifi==2022.9.24\n",
    "cffi==1.15.1\n",
    "charset-normalizer==2.1.1\n",
    "colorama==0.4.5\n",
    "colorcet==3.0.1\n",
    "contourpy==1.0.5\n",
    "cycler==0.11.0\n",
    "debugpy==1.6.3\n",
    "decorator==5.1.1\n",
    "defusedxml==0.7.1\n",
    "dill==0.3.5.1\n",
    "entrypoints==0.4\n",
    "executing==1.1.1\n",
    "fastjsonschema==2.16.2\n",
    "flake8==5.0.4\n",
    "fonttools==4.37.4\n",
    "gitdb==4.0.9\n",
    "GitPython==3.1.29\n",
    "greenlet==1.1.3.post0\n",
    "holoviews==1.15.1\n",
    "idna==3.4\n",
    "ipykernel==6.16.0\n",
    "ipython==8.5.0\n",
    "ipython-genutils==0.2.0\n",
    "ipywidgets==8.0.2\n",
    "isort==5.10.1\n",
    "jedi==0.18.1\n",
    "Jinja2==3.1.2\n",
    "json5==0.9.10\n",
    "jsonschema==4.16.0\n",
    "jupyter-core==4.11.1\n",
    "jupyter-server==1.19.1\n",
    "jupyter-server-mathjax==0.2.6\n",
    "jupyter_client==7.3.5\n",
    "jupyterlab==3.4.8\n",
    "jupyterlab-pygments==0.2.2\n",
    "jupyterlab-widgets==3.0.3\n",
    "jupyterlab_server==2.15.2\n",
    "kiwisolver==1.4.4\n",
    "lazy-object-proxy==1.7.1\n",
    "Markdown==3.4.1\n",
    "MarkupSafe==2.1.1\n",
    "matplotlib==3.6.1\n",
    "matplotlib-inline==0.1.6\n",
    "mccabe==0.7.0\n",
    "mistune==2.0.4\n",
    "msgpack==1.0.4\n",
    "nbclassic==0.4.5\n",
    "nbclient==0.7.0\n",
    "nbconvert==7.2.1\n",
    "nbdime==3.1.1\n",
    "nbformat==5.7.0\n",
    "nest-asyncio==1.5.6\n",
    "notebook==6.4.12\n",
    "notebook-shim==0.1.0\n",
    "numpy==1.23.3\n",
    "packaging==21.3\n",
    "pandas==1.5.0\n",
    "pandocfilters==1.5.0\n",
    "panel==0.14.0\n",
    "param==1.12.2\n",
    "parso==0.8.3\n",
    "pep8-naming==0.13.2\n",
    "pexpect==4.8.0\n",
    "pickleshare==0.7.5\n",
    "Pillow==9.2.0\n",
    "platformdirs==2.5.2\n",
    "prometheus-client==0.14.1\n",
    "prompt-toolkit==3.0.31\n",
    "psutil==5.9.2\n",
    "ptyprocess==0.7.0\n",
    "pure-eval==0.2.2\n",
    "pycodestyle==2.9.1\n",
    "pycodestyle_magic==0.5\n",
    "pycparser==2.21\n",
    "pyct==0.4.8\n",
    "pyflakes==2.5.0\n",
    "Pygments==2.13.0\n",
    "pylint==2.15.4\n",
    "pynvim==0.4.3\n",
    "pyparsing==3.0.9\n",
    "pyrsistent==0.18.1\n",
    "python-dateutil==2.8.2\n",
    "pytz==2022.4\n",
    "pyviz-comms==2.2.1\n",
    "PyYAML==6.0\n",
    "pyzmq==24.0.1\n",
    "requests==2.28.1\n",
    "scipy==1.9.2\n",
    "Send2Trash==1.8.0\n",
    "six==1.16.0\n",
    "smmap==5.0.0\n",
    "sniffio==1.3.0\n",
    "soupsieve==2.3.2.post1\n",
    "stack-data==0.5.1\n",
    "terminado==0.16.0\n",
    "tinycss2==1.1.1\n",
    "tomli==2.0.1\n",
    "tomlkit==0.11.5\n",
    "toolz==0.12.0\n",
    "tornado==6.2\n",
    "tqdm==4.64.1\n",
    "traitlets==5.4.0\n",
    "typing_extensions==4.4.0\n",
    "urllib3==1.26.12\n",
    "vega-datasets==0.9.0\n",
    "wcwidth==0.2.5\n",
    "webencodings==0.5.1\n",
    "websocket-client==1.4.1\n",
    "widgetsnbextension==4.0.3\n",
    "wrapt==1.14.1\n",
    "```\n",
    "\n",
    "This list contains all packages that are currently installed in my environment, complete with the exact version number.\n",
    "\n",
    "Now, you or somebody with whom you shared your code could re-create your environment by typing out single `pip install` commands following this list, but fortunately, there is a better way.\n",
    "\n",
    "Type into your terminal:\n",
    "\n",
    "```bash\n",
    "pip freeze > requirements.txt\n",
    "```\n",
    "\n",
    "This will redirect the output of `pip freeze` into the text file `requirements.txt`. You can install all dependencies listed in this file at once using the command:\n",
    "\n",
    "```bash\n",
    "pip install -r requirements.txt\n",
    "```\n",
    "\n",
    "**Including a requirements.txt in your project directory is a central step** in making your code shareable and re-useable by others. Not to mention that it may save yourself a lot of headaches."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note: for environments managed by the package manager [Conda](https://docs.conda.io/projects/conda/en/stable/), the output of `pip freeze` may not produce a valid `requirements.txt` format but something like this:\n",
    "\n",
    "```\n",
    "...\n",
    "requests @ file:///home/conda/feedstock_root/build_artifacts/requests_1684774241324/work\n",
    "requests-oauthlib==1.3.1\n",
    "rfc3339-validator @ file:///home/conda/feedstock_root/build_artifacts/rfc3339-validator_1638811747357/work\n",
    "rfc3986-validator @ file:///home/conda/feedstock_root/build_artifacts/rfc3986-validator_1598024191506/work\n",
    "rope==1.12.0\n",
    "...\n",
    "```\n",
    "\n",
    "When this happens\n",
    "\n",
    "```bash\n",
    "pip list --format=freeze > requirements.txt\n",
    "```\n",
    "\n",
    "will get rid of the file references and produce the correct format.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Registering an environment with Jupyter\n",
    "\n",
    "If you created a new environment, you will find that it does not automatically show up in Jupyter. How can you make the new environment available?\n",
    "\n",
    "First, your new environment must be able to communicate with the Jupyter server. For this purpose, you have to install the `ipykernel` package through `pip install ipykernel`. Then, you have to tell Jupyter to register your environment:\n",
    "\n",
    "> ```bash\n",
    "> python -m ipykernel install --name=\"my-environment\" --user\n",
    "> ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(Note that the environment needs to be active, of course. If you used the folder name \"env\", your command prompt should indicated the active environment by showing `(env)`.)\n",
    "\n",
    "For the environment to show up in Jupyter, you may need to press F5 to refresh the website."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Removing an environment\n",
    "\n",
    "You can remove an environment simply be deleting the respective folder, but\n",
    "Jupyter will not automatically know that the environment has vanished.\n",
    "You can remove it from Jupyter like so:\n",
    "\n",
    "> ```bash\n",
    "> jupyter kernelspec remove \"my-environment\"\n",
    "> ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "</div>\n",
    "\n",
    "- Create your own environment and register it with Jupyter\n",
    "- Create a new notebook that uses the new environment as kernel\n",
    "- Try to import numpy; install numpy into the environment, restart the notebook and try again\n",
    "- Verify that you use the correct Python executable by running the following code\n",
    "\n",
    "> ```python\n",
    "> import sys\n",
    "> sys.executable\n",
    "> ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Project creation workflow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A good idea is to include a requirements.txt right from the start and update it whenever you add or update a package. A typical workflow when creating a new Python project may thus look like this:\n",
    "\n",
    "```bash\n",
    "mkdir my-project\n",
    "cd my-project\n",
    "echo \"# My awesome project\" > README.md\n",
    "mkdir data notebooks\n",
    "python3 -m venv env\n",
    "source env/bin/activate\n",
    "pip install bokeh ipykernel pandas scipy\n",
    "python -m ipykernel install --name=\"my-project\" --user\n",
    "pip freeze > requirements.txt\n",
    "```\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This gets you started with a basic folder structure, readme file, a dedicated environment, a jupyter kernel, and some basic scientific packages. If you create projects regularly, you may want to save the empty folder structure as a template. Alternatively, you can write a script or use a tool like [&rarr; cookiecutter](https://cookiecutter.readthedocs.io/en/stable/) to create a project with a good layout."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Version numbers\n",
    "\n",
    "To conclude this introduction into environments, let's look a bit more closely at the output of `pip freeze`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that most of the package version numbers consist of three parts, `x.y.z`. This numbering scheme is called [&rarr; semantic versioning](https://packaging.python.org/guides/distributing-packages-using-setuptools/#semantic-versioning-preferred) and it helps you to understand in what way an update of a package changes its functionality."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first number, the **major version number**, indicates **backwarts compatibility**. If, say, you are working with numpy v1.17 and tomorrow version 2.0 is released, it may be that some of your code that uses numpy does not work anymore, because numpy 2.0 is not fully backwarts compatible. If the major version of a package changes, you should **have a look at the release notes**. For example, for scipy you would find that quite some functions were removed when the package (finally) [&rarr; made its jump to v1.0.0](https://docs.scipy.org/doc/scipy/release.1.0.0.html#backwards-incompatible-changes)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second number, the **minor version number**, indicates that **new functionality was added** in a way that *is* (or should be) backwarts compatible. Old code should still work fine with a package that has recieved some minor updates since the code was written."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last number, the **maintenance version number**, indicates bug-fixes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So what is up with some packages with version 0.x.y? **Major release 0** usually indicates that the **developers do not want to commit to a specific [API](glossary.md#API) yet**, and that **backwards incompatible changes can happen more often**. It does not necessarily mean that the package is not mature or buggy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another version scheme is date based. `pip` itself uses date based versioning. Currently, the version is \"22.2.2\" (you can type `pip --version` in the terminal to see this), indicating that the release is from 20**22**, not that it is the 21th major release. While it is nice to know how old a package is, this scheme does not inform about API compatibility."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Looking Beyond ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We only scratched the surface here in terms of project and environment management. Other tools that go a little further are the packaging tools [Pipenv](https://pipenv.pypa.io/en/latest/), [Poetry](https://python-poetry.org/), or of course [Conda](https://docs.conda.io/projects/conda/en/latest/).\n",
    "\n",
    "Some differences are the notion of \"locking\" dependencies, which means that a [hash value](glossary.md#Hash) is calculated from the source code of all depenedencies (Pipenv and Poetry do that) and all transient dependencies (dependencies of your dependencies). On the other hand, some packages have non-python dependencies that are not managed by Pipenv/Peotry/pip. For example, Jupyter requires [Node.JS](glossary.md#NodeJS-🤓) to run, which is an external program, not a Python package. Conda can include such non-python dependencies.\n",
    "An even safer way is the use of [Docker](https://www.docker.com/) containers (one can think of containers as light-weight virtual machines), which is something professional software developers like to use.\n",
    "\n",
    "A good way to install Python programs (not programming libraries) automatically in their own dedicated environments is to use [pipx](https://pypa.github.io/pipx/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: version-control.ipynb](version-control.ipynb)"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  },
  "toc-autonumbering": false,
  "toc-showcode": false,
  "toc-showmarkdowntxt": false,
  "toc-showtags": false
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
