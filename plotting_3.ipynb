{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Interactive plots (III)\n",
    "\n",
    "... continues [plotting_2.ipynb](plotting_2.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"tblcts\">*Table of contents*</a>\n",
    "\n",
    "- [A more complex dashboard](#A-more-complex-dashboard)\n",
    "- [Altair and other plotting libraries](#Altair-and-other-plotting-libraries)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A more complex dashboard\n",
    "\n",
    "I want to show you how a more complex GUI (a so-called dashboard) may be put together.\n",
    "I will not comment this code in great detail, but leave it to you as an exercise to look up the bits and pieces that we did not discuss in the previous notebooks. Do not worry, we covered most of the things."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The figure I am going to prepare is about global health and is built from two parts: A scatter plot shows how long people live (x-axis) in different parts of the world (color) and how much money is spent per citizen each year on health (y-axis). The size of the marks encodes the population of each country. The second part of the figure is a time series for a selected country (life expectancy at birth versus year). The figure is inspired by [&rarr; Hans Rosling's](https://en.wikipedia.org/wiki/Hans_Rosling) excellent [&rarr; data visualisations](https://www.youtube.com/watch?v=hVimVzgtD6w&t=2m30s)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's get going:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prepare the kernel\n",
    "\n",
    "from functools import lru_cache\n",
    "\n",
    "import ipywidgets as widgets\n",
    "import numpy as np\n",
    "from bokeh.io import push_notebook\n",
    "from bokeh.layouts import gridplot\n",
    "from bokeh.models import Div, HoverTool\n",
    "from bokeh.palettes import Category10_6\n",
    "from bokeh.plotting import ColumnDataSource, figure, output_notebook, show\n",
    "\n",
    "from script.prepare_who_data import get_WHO_data_bokeh_dashboard\n",
    "\n",
    "output_notebook()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of dictionaries, we work with `pandas` dataframes as input for `ColumnDataSource`, see the [tabular-data](tabular-data_1.ipynb) notebooks. Here is the full dataset that we require ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data, WHO_data_year_averaged = get_WHO_data_bokeh_dashboard()\n",
    "WHO_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and here a dataset where I averaged over the years:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WHO_data_year_averaged"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that we are missing some countries. This is because of missing data or formatting problems in the original data sources. For this example I did not address these issues, but one should do so in a real research project."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# this is needed as input for some selection widgets\n",
    "COUNTRIES = set(WHO_data.Country)\n",
    "# and this is needed to map a region to a color\n",
    "REGIONS = sorted(set(WHO_data.Region))\n",
    "REGION_COLOR_DICT = {k: v for k, v in zip(sorted(REGIONS), Category10_6)}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need two more columns in our dataframe, one holding the size which corresponds to the population and one which holds the color that corresponds to the region:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for df in (WHO_data, WHO_data_year_averaged):\n",
    "    # we work with circles, their area should scale with the population\n",
    "    df[\"size\"] = np.sqrt(df[\"Population in 2016 (thousands)\"] / np.pi) / 10\n",
    "    df[\"color\"] = df[\"Region\"].map(lambda r: REGION_COLOR_DICT[r])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can prepare the figure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# initialize figures\n",
    "\n",
    "WHO_scatter_plot = figure(\n",
    "    height=400,\n",
    "    width=500,\n",
    "    y_axis_type=\"log\",\n",
    "    x_axis_label=\"life expectancy at birth / years\",\n",
    "    y_axis_label=\"yearly health expediture / US$\",\n",
    "    x_range=(35, 90),\n",
    "    y_range=(1, 15000),\n",
    "    title=\"averaged 2000-2015\",\n",
    ")\n",
    "\n",
    "WHO_scatter_plot.title.text_font_size = \"14pt\"\n",
    "\n",
    "WHO_timeseries_plot = figure(\n",
    "    height=400,\n",
    "    width=500,\n",
    "    x_axis_label=\"year\",\n",
    "    y_axis_label=\"life expectancy at birth / years\",\n",
    "    title=\"Germany\",\n",
    ")\n",
    "\n",
    "WHO_timeseries_plot.title.text_font_size = \"14pt\"\n",
    "\n",
    "\n",
    "# prepare scatter plot\n",
    "\n",
    "WHO_scatter_source = ColumnDataSource(WHO_data_year_averaged)\n",
    "\n",
    "WHO_scatter_plot.scatter(\n",
    "    x=\"Life expectancy, both sexes\",\n",
    "    y=\"Health expenditure /US$\",\n",
    "    size=\"size\",\n",
    "    source=WHO_scatter_source,\n",
    "    color=\"color\",\n",
    "    alpha=0.5,\n",
    ")\n",
    "\n",
    "# add a tooltip\n",
    "\n",
    "hovertool = HoverTool()\n",
    "hovertool.tooltips = [\n",
    "    (\"Country\", \"@Country\"),\n",
    "    (\"Life expectency, both sexes\", \"@{Life expectancy, both sexes}\"),\n",
    "    (\"Yearly expediture / US$\", \"@{Health expenditure /US$}\"),\n",
    "    (\"Region\", \"@Region\"),\n",
    "]\n",
    "WHO_scatter_plot.add_tools(hovertool)\n",
    "\n",
    "\n",
    "# prepare timeseries plot\n",
    "\n",
    "WHO_timeseries_source = ColumnDataSource(WHO_data[WHO_data[\"Country\"] == \"Germany\"])\n",
    "\n",
    "WHO_timeseries_plot.line(\n",
    "    x=\"Year\", y=\"Life expectancy, both sexes\", source=WHO_timeseries_source\n",
    ")\n",
    "\n",
    "temp_df = WHO_data[WHO_data[\"Country\"] == \"Germany\"]\n",
    "year_indicator_source = ColumnDataSource(temp_df[temp_df[\"Year\"] == 2000])\n",
    "WHO_timeseries_plot.scatter(\n",
    "    x=\"Year\",\n",
    "    y=\"Life expectancy, both sexes\",\n",
    "    source=year_indicator_source,\n",
    "    color=\"red\",\n",
    "    size=10,\n",
    ")\n",
    "\n",
    "# create bokeh widget that will be display as color legend\n",
    "# (uses raw html)\n",
    "color_legend = Div(\n",
    "    styles={\"padding-top\": \"20px\"},\n",
    "    text=\"<br>\".join(\n",
    "        f'<span style=\"color: {c}\">{r}</span>' for r, c in REGION_COLOR_DICT.items()\n",
    "    ),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And the widgets to interact with the figure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "year_options = dict(min=2000, max=2015, step=1)\n",
    "play_scatter = widgets.Play(interval=250, **year_options)\n",
    "slider_scatter = widgets.IntSlider(description=\"Year\", **year_options)\n",
    "reset_button = widgets.Button(description=\"averaged\")\n",
    "select_timeseries = widgets.Dropdown(\n",
    "    description=\"timeseries\", options=sorted(COUNTRIES), value=\"Germany\"\n",
    ")\n",
    "highlight_button = widgets.ToggleButton(description=\"highlight country\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before declaring the callback functions, we define some helper functions to retrieve the required data. In this way, the callback functions become less convoluted and we can [cache the results for better performance](performance.ipynb#Caching)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# data retrieval functions\n",
    "\n",
    "\n",
    "@lru_cache(maxsize=4096)\n",
    "def get_data_dict_for_year(year):\n",
    "    return dict(ColumnDataSource(WHO_data[WHO_data[\"Year\"] == year]).data)\n",
    "\n",
    "\n",
    "@lru_cache(maxsize=4096)\n",
    "def get_data_dict_for_country(country):\n",
    "    return dict(ColumnDataSource(WHO_data[WHO_data[\"Country\"] == country]).data)\n",
    "\n",
    "\n",
    "@lru_cache(maxsize=4096)\n",
    "def get_data_dict_for_year_and_country(year, country):\n",
    "    df = WHO_data[WHO_data[\"Country\"] == country]\n",
    "    df = df[df[\"Year\"] == year]\n",
    "    return dict(ColumnDataSource(df).data)\n",
    "\n",
    "\n",
    "def get_colors(country=None):\n",
    "    regions = WHO_scatter_source.data[\"Region\"]\n",
    "    countries = WHO_scatter_source.data[\"Country\"]\n",
    "    region_colors = np.array([REGION_COLOR_DICT[r] for r in regions])\n",
    "    if country is not None:\n",
    "        return np.where(countries == country, region_colors, \"#cccccc\")\n",
    "    else:\n",
    "        return region_colors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the callback functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# callback functions\n",
    "\n",
    "\n",
    "def update_WHO_scatter_plot(change):\n",
    "\n",
    "    year = slider_scatter.value\n",
    "    country = select_timeseries.value\n",
    "\n",
    "    # update scatter plot\n",
    "    WHO_scatter_plot.title.text = str(year)\n",
    "    WHO_scatter_source.data = get_data_dict_for_year(year)\n",
    "\n",
    "    # update time series plot\n",
    "    WHO_timeseries_plot.title.text = country\n",
    "    # update year indicator\n",
    "    year_indicator_source.data = get_data_dict_for_year_and_country(year, country)\n",
    "\n",
    "    # update color\n",
    "    highlight_country = country if highlight_button.value else None\n",
    "    WHO_scatter_source.data[\"color\"] = get_colors(highlight_country)\n",
    "\n",
    "    if change[\"owner\"] == select_timeseries:\n",
    "        # update time series\n",
    "        WHO_timeseries_source.data = get_data_dict_for_country(country)\n",
    "\n",
    "    push_notebook()\n",
    "\n",
    "\n",
    "def reset_WHO_scatter_plot(change):\n",
    "    src = ColumnDataSource(WHO_data_year_averaged)\n",
    "    WHO_scatter_plot.title.text = \"averaged 2000-2015\"\n",
    "    WHO_scatter_source.data = dict(src.data)\n",
    "    push_notebook()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we link our widgets to the callback functions ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reset_button.on_click(reset_WHO_scatter_plot)\n",
    "play_scatter.observe(update_WHO_scatter_plot)\n",
    "slider_scatter.observe(update_WHO_scatter_plot)\n",
    "widgets.jslink((play_scatter, \"value\"), (slider_scatter, \"value\"))\n",
    "select_timeseries.observe(update_WHO_scatter_plot)\n",
    "highlight_button.observe(update_WHO_scatter_plot)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and display the figure and widgets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show(\n",
    "    gridplot([[WHO_scatter_plot, WHO_timeseries_plot, color_legend]]),\n",
    "    notebook_handle=True,\n",
    ")\n",
    "\n",
    "widgets.HBox(\n",
    "    [slider_scatter, reset_button, play_scatter, select_timeseries, highlight_button]\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Go ahead and see what you can find out about the global health development. Some countries show severe drops in life expectancy. What happend there?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Altair and other plotting libraries\n",
    "\n",
    "To conclude this section of the course and just to showcase how different the plotting workflow can feel when we switch the tool, I re-created the figure above using [Altair](https://altair-viz.github.io/). This can server as a starting point for us to get a small overview on the alternatives to Bokeh that I mentioned in the introduction of [plotting_1.ipynb](plotting_1.ipynb):\n",
    "\n",
    "- [Altair](https://altair-viz.github.io/) ([PyCon 2018 tutorial on Youtube](https://www.youtube.com/watch?v=ms29ZPUKxbU))\n",
    "- [Bokeh](https://docs.bokeh.org/en/latest/index.html) ([PyCon 2017 tutorial on YouTube](https://www.youtube.com/watch?v=xId9B1BVusA))\n",
    "- [bqplot](https://bqplot.readthedocs.io/en/latest/) ([SciPy 2018 talk on YouTube](https://www.youtube.com/watch?v=Dmxa2Kyfzxk))\n",
    "- [HoloViews](https://holoviews.org/index.html) ([SciPy 2019 tutorial on YouTube](https://www.youtube.com/watch?v=7deGS4IPAQ0))\n",
    "- [Matplotlib](https://matplotlib.org/) ([SciPy 2019 tutorial on YouTube](https://www.youtube.com/watch?v=Tr4DYo4v5AY))\n",
    "- [Plotly](https://plot.ly/) ([PyData 2018 talk on YouTube](https://www.youtube.com/watch?v=cuTMbGjN2GQ))\n",
    "- [Seaborn](https://seaborn.pydata.org/index.html) ([PyData 2017 tutorial on YouTube](https://www.youtube.com/watch?v=KvZ2KSxlWBY))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import altair as alt\n",
    "\n",
    "# create country selection\n",
    "select_year_dropdown = alt.binding_range(\n",
    "    min=2000, max=2015, step=1, name=\"select year: \"\n",
    ")\n",
    "select_year = alt.selection_point(\n",
    "    fields=[\"Year\"], bind=select_year_dropdown, clear=False\n",
    ")\n",
    "\n",
    "# create year selection\n",
    "select_country_dropdown = alt.binding_select(\n",
    "    options=sorted(COUNTRIES), name=\"select country: \"\n",
    ")\n",
    "select_country = alt.selection_point(\n",
    "    fields=[\"Country\"], bind=select_country_dropdown, clear=False\n",
    ")\n",
    "\n",
    "# create scatter plot\n",
    "altair_scatter = (\n",
    "    alt.Chart(WHO_data, title=\"All countries\")\n",
    "    .mark_circle()\n",
    "    .encode(\n",
    "        x=alt.X(\"Life expectancy, both sexes\", scale=alt.Scale(zero=False)),\n",
    "        y=alt.Y(\"Health expenditure /US$\", scale=alt.Scale(type=\"log\")),\n",
    "        color=alt.condition(select_country, \"Region\", alt.value(\"lightgrey\")),\n",
    "        size=alt.Size(\n",
    "            \"Population in 2016 (thousands)\", scale=alt.Scale(range=[25, 2000])\n",
    "        ),\n",
    "        tooltip=[\"Country\", \"Region\", \"Population in 2016 (thousands)\"],\n",
    "    )\n",
    "    .add_params(\n",
    "        select_country,\n",
    "    )\n",
    "    .transform_filter(select_year)\n",
    "    .interactive()\n",
    ")\n",
    "\n",
    "# create timeseries plot\n",
    "altair_line = (\n",
    "    alt.Chart(WHO_data, title=\"Timeseries selected country\")\n",
    "    .mark_line(point=True)\n",
    "    .encode(\n",
    "        x=alt.X(\"Year\", scale=alt.Scale(zero=False)),\n",
    "        y=alt.Y(\"Life expectancy, both sexes\", scale=alt.Scale(zero=False)),\n",
    "    )\n",
    "    .add_params(select_year)\n",
    "    .transform_filter(select_country)\n",
    ")\n",
    "\n",
    "# display plots\n",
    "altair_scatter | altair_line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: tabular-data_1.ipynb](tabular-data_1.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
