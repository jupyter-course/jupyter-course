# Special Methods Course

## Notes on Homework

I forgot to mention it, but good for you to keep in mind for the future:

Include a _How to run_ section in your README.md,
for example:

## How to Run

To run the project, first create a new environment inside the project folder:

```bash
$ cd my-project
$ python -m venv env
```

Activate the environment and install the required Python packages from the provided `requirements.txt` file:

```bash
$ source env/bin/activate
(env) $ pip install -r requirements.txt
```

Install the kernel for Jupyter:

```bash
(env) $ pyhton -m ipykernel intall --name my-project --user
```

## Binder Example

Check out 

https://gitlab.gwdg.de/nluetts/test-repo

to find a small example on how to run a repository on binder.

## Evaluation

Here is room for your feedback.

Make sure that you are not logged in to your GWDG account when editing and it will be anonymous.