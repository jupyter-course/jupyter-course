{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Unittesting and debugging"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2022-10-12\n",
    "\n",
    "N. Luettschwager"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"tableofcontents\">*Table of contents*</a>\n",
    "\n",
    "- [Assertions](#Assertions)\n",
    "- [The unittest module](#The-unittest-module)\n",
    "  - [The TestCase](#The-TestCase)\n",
    "  - [Auto-discover](#Auto-discover)\n",
    "  - [Unit test output](#Unit-test-output)\n",
    "  - [More assertions, setUp(), and tearDown()](#More-assertions,-setUp(),-and-tearDown())\n",
    "  - [Advanced: Mocking](#Advanced:-Mocking)\n",
    "- [Exercise](#excs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous notebook [we discussed](style.ipynb#Code-structure) that code should be build from blocks, i.e. logical and ideally small units that server a single purpose. These blocks are functions, classes or modules. I explained the benefits of this approach and stated that this will also enhance testability. In this notebook we discuss a fundamental way of testing your building blocks: the *unittest*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In one way or the other, code is tested in any case. The bare minimum is that you run a script, verify that no error is thrown, and compare the results with your general expectation. To test your functions and classes, you can open a [REPL](glossary.md#REPL), import them, prepare some state, and run them. You view the results and see if they are what you expected.\n",
    "If you want to check intermediate results, you throw in a lot of print statements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now imagine that you want to share your code with somebody else, you want to use the code on a different machine, or you simply want to update some package that your code depends on. You will want to test your code again, to make sure everything still works as expected in the different environement. What is supposed to happen now? Laborious manual testing of the code again? This may be okay for very small projects, but becomes intractable if the project is large and has a lot if intertwined parts."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the remedies of this problem is *automated* unittesting. Essentially, a unittest is a function that does the test for you, having the obvious benefit that it can later just be re-run to repeat the test. These are the steps that are typically included in a unittest:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- prepare some state, for example:\n",
    "    - define variables that serve as input for the function/method under test\n",
    "    - create a class instance and run some methods\n",
    "    - create temporary files\n",
    "    - establish a database connection\n",
    "- call the function/method under test with that specific state\n",
    "- compare the results with your expectations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Assertions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A central question for unittesting seems to be how to programmatically ask Python to compare some results to some expectations. A solution that may come to mind could be something like this:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# in file my_module.py\n",
    "\n",
    "def get_foo(arguments):\n",
    "    # do something with the arguments\n",
    "    ...\n",
    "    return foo\n",
    "\n",
    "\n",
    "# in file test_my_module.py\n",
    "\n",
    "from my_module import get_foo\n",
    "\n",
    "foo = get_foo()\n",
    "if foo != 'foo':\n",
    "    raise ValueError(f\"get_foo() did not return foo, got {foo} instead.\")\n",
    "    \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this setup, we would run our tests simply by executing `test_my_module.py`. If we get an error, we know something is different than we expected."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last pattern in the fictional file `test_my_module.py`&mdash;testing if some condition is met/not met and throwing an error otherwise&mdash;is compiled in Python's `assert` statements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assert 3 > 2  # verify that 3 is bigger than 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running the cell above seems not to be doing anything. That is because the predicate after the `assert` keyword evaluates to `True`.\n",
    "`assert` works like so: If whatever is stated after the `assert` keyword evaluates to `True`, proceed silently, otherwise, throw an `AssertionError`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assert 'baz' in 'foobar'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assert False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Therefore, we can use `assert` in `test_my_module.py` to get rid of the if-statement:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# in test_my_module.py\n",
    "\n",
    "foo = get_foo()\n",
    "assert foo == 'foo'\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we are missing the informative error message (if the assertion fails, we will get a plain `AssertionError`).\n",
    "We can get the error message back by adding it to the assert statement, separated by a comma:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# in test_my_module.py\n",
    "\n",
    "foo = get_foo()\n",
    "assert foo == 'foo', f\"get_foo() did not return foo, got {foo} instead.\"\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's test it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_foo():\n",
    "    return 'foo'\n",
    "\n",
    "foo = get_foo()\n",
    "assert foo == 'foo', f\"get_foo() did not return foo, got {foo} instead.\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_foo():\n",
    "    return 'bar'\n",
    "\n",
    "foo = get_foo()\n",
    "assert foo == 'foo', f\"get_foo() did not return foo, got {foo} instead.\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The unittest module"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the example above, we wrote a separate Python file and implemented some tests by simply calling functions from the global scope of the script.\n",
    "This is a bad idea, since after many tests which all need their own steps to prepare some state, we are left with an ill-defined global state.\n",
    "We can solve this problem by letting each test run in the [local scope](glossary.md#Local-and-global-variables) of a separate function:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "def test_get_foo():\n",
    "    foo = get_foo()\n",
    "    assert foo == 'foo', f\"get_foo() did not return foo, got {foo} instead.\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this code, the [local variable](glossary.md#Local-and-global-variables) `foo` is only accessible within the `test_get_foo()` function and does not pollute the global [namespace](glossary.md#Namespace) and alter the global state. But what happens if we have several similar tests that involve the same set-up of some particular state?\n",
    "\n",
    "Well, we could write a function that prepares that particular state for us and use it accross several tests.\n",
    "However, at this point we find ourselves reinventing the wheel. Python includes tools for running automated unittests which simplify testing for us: the module `unittest`, a part of the standard library, has us covered."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The TestCase"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `unittest` module provides the class `TestCase` around which unit tests are build.\n",
    "\n",
    "Here is how this would look like for our `get_foo()` function: <a id=\"unittest-template\"></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import unittest\n",
    "\n",
    "\n",
    "class TestGetFoo(unittest.TestCase):\n",
    "\n",
    "    def test_get_foo(self):\n",
    "        foo = get_foo()\n",
    "        self.assertEqual(foo, 'foo')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The basic recipe is:\n",
    "\n",
    "1. You declare a test class that inherits from `unittest.TestCase`.\n",
    "1. You write test methods that perform some set-up and run the function under test (the name of these methods must start with `test`, otherwise they are ignored)\n",
    "1. Inside the test methods, you check the effect and/or return value(s) of the function by one or several assertion statements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It may not yet be clear why this approach is better than what we have done so far. To show the benefits of `unittest`, I wrote a small example module and associated tests: [examples/unittest/integrate/integrate.py](examples/unittest/integrate/integrate.py) and [examples/unittest/test/test_integrate.py](examples/unittest/test/test_integrate.py)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In my example, the function under test, `integrate_trapezoidal()`, performs numerical integration of a one-dimensional curve, given by discrete x-y data. Besides returning the value of the integral, it also prints the result if the keyword argument `display_result=True` is passed in. We can test the return value of this function realtively easily: with mathematical functions for which we can calculate the integral analytically."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Auto-discover"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before discussing the code that performs the actual tests, I want to show you how to run it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You simply have to (from the root directory of the jupyter-course):\n",
    "\n",
    "```bash\n",
    "source env/bin/activate\n",
    "cd examples/unittest\n",
    "python -m unittest\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can do this from the notebook, using the %%bash magic function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%bash --no-raise-error\n",
    "\n",
    "source env/bin/activate\n",
    "cd examples/unittest/\n",
    "python -m unittest"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in the commands above, I did not have to specify which Python file to run. Instead, I simply ran the `unittest` module as a script by calling it with the `-m` flag. If I do this from within a directory that contains tests, the `unittest` module is able to find and run the tests automatically. It *auto-discovers* the tests. If I would have several files in the `unittest/test` folder, they would be run as well.\n",
    "\n",
    "To run a specific test, you have to specify its path using a dot-notation like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash --no-raise-error\n",
    "\n",
    "source env/bin/activate\n",
    "cd examples/unittest/\n",
    "python -m unittest test.test_integrate.TestIntegrateTrapezoidal.test_constant_function_close_to"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Unit test output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have a look at the output from the first run of `unittest` (including all tests). The first line\n",
    "\n",
    "`F.FF.E`\n",
    "\n",
    "shows that 6 test were run in total. Two tests passed (`.`), three failed (`F`), and one caused an error (`E`).\n",
    "\"Fails\" means that an assert statement was violated (an `AssertionError` was thrown).\n",
    "If everything had been fine, the output should have looked like this:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "......\n",
    "----------------------------------------------------------------------\n",
    "Ran 6 test in 0.016s\n",
    "\n",
    "OK\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below the first line which summarizes the `unittest` run, we get detailed information about each test function that failed or produced an error, including why a particular assertion failed (expected versus actual result).\n",
    "\n",
    "The test that throws an error (`test_throws_error`) was deliberately set up by me to demonstrate the behavior in case an exception other than `AssertionError` occurs. `test_constant_function` and `test_quadratic_function_close_to` seem to indicate a problem with numerical accuracy, which is to be expected from a numerical integration method.\n",
    "The failure of `test_linear_function_x_reversed` exposes a bug in our integration function that needs attention."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### More assertions, setUp(), and tearDown()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have a look at [test_integrate.py](examples/unittest/test/test_integrate.py), you see that it follows the basic template that I showed [above](#unittest-template). The test case holds several methods that test the integration function with different data derived from a constant function and a second order polynomial function. The constant function is an easy test case, because we know the integral must be $c \\times \\Delta x$. The integral of the second order polynomial is only slightly more difficult to compute."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have a look at `test_constant_function()`.\n",
    "You may have noted, that we did not assert the return value like we discussed in the beginning:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "assert integral == 50, \"something went wrong ...\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but we did:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "self.assertEqual(integral, 50, \"something went wrong ...\")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `assertEqual()` method is provided by `TestCase`. There are many other useful build-in assertions. For example:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- `assertNotEqual(a, b)`\n",
    "- `assertTrue(x)`\n",
    "- `assertFalse(x)`\n",
    "- `assertIsNone(x)`\n",
    "- `assertIsNotNone(x)`\n",
    "- `assertIn(a, b)`\n",
    "- `assertNotIn(a, b)`\n",
    "- `assertAlmostEqual(a, b)`\n",
    "- and more.\n",
    "\n",
    "You find the full list in the `TestCase` [&rarr; reference manual](https://docs.python.org/3/library/unittest.html?highlight=unittest#unittest.TestCase)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In case of our integration function, the `assertAlmostEqual()` method is especially helpful. It can be customized to check whether two numbers are equal within a particular precision, which defaults to 7 digits. Using this assertion, the test method `test_constant_function_close_to` passes, because the integration result is close enough to the expected result.\n",
    "In `test_quadratic_function_close_to`, the deviation is larger and the assertion still fails. Note how nicely the failure is presented to us by `assertAlmostEqual()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Besides the specialized assertions and their nicely formatted messages, what else does `TestCase` have in stock for us?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another essential feature is the possibility to prepare a particular state for our test methods. The [&rarr; `setUp()` method](https://docs.python.org/3/library/unittest.html?highlight=unittest#unittest.TestCase.setUp) from the `TestCase` class is called automatically before each test method is run.<sup>1</sup> I used it to prepare x-values that I can use in (almost) all methods. More usecases are setting up temporary files or connecting to a database, for example. The [&rarr; `tearDown()` method](https://docs.python.org/3/library/unittest.html?highlight=unittest#unittest.TestCase.tearDown) is called automatically *after* each test method. It can be used to undo whatever was done in `setUp()`. I had no reason to use it here.\n",
    "\n",
    "---\n",
    "<sup>1</sup> In terms of performance it would be better to use the [&rarr; `setUpClass()` method](https://docs.python.org/3/library/unittest.html?highlight=unittest#unittest.TestCase.setUpClass) here. However, this requires us to discuss classmethods and class versus instance variables, which I want to avoid at this point. If you are interested, we can talk about it in the course."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Advanced: Mocking"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Quite deliberately, above I wrote that it is relatively easy to test the *return value* of our integration function.\n",
    "What about it displaying the result on the screen? Granted, this test is rather trivial in our case&mdash;of course the print function will be called&mdash;but in a real world application you might want to test if `print()` (or some logging function) is really executed in a more complex situation, and that the string that is displayed indeed conforms with the format you intended."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is where the `unittest.mock` module comes in. It provides means to *overwrite* functions or objects and then probe if they/their methods were called and what the call's arguments and keyword arguments were.\n",
    "I will touch mocking only very briefly and restrict the discussion to overwriting (patching) functions using the [&rarr; `patch` decorator](https://docs.python.org/3/library/unittest.mock.html#unittest.mock.patch).\n",
    "The last test method in [test_integrate.py](examples/unittest/test/test_integrate.py), `test_shows_correct_result`, shows an example of how to use `patch`. A generic template is:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "@patch(\"<path.to.function.in.dot.notation>\", return_value=<value that you need>)\n",
    "def my_test(self, patch_object):\n",
    "    # do something\n",
    "    ...\n",
    "    patch_object.assert_called_with(<your arguments/keyword arguments>)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that when using `@patch()`, you have to pass a `patch_object` (the name does not matter) to the test method. `patch_object` represents the function that was replaced. It allows you to assert certain things about the patched function, for example if it was called at all, how often it was called, and, as in my example, with what arguments it was called. The associated assert methods of the patch object are listed in the [&rarr; mock documentation](https://docs.python.org/3/library/unittest.mock.html#unittest.mock.Mock.assert_called)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using mocking to replace `print()` is, as I said, quite trivial. It is much more useful if you want to test parts of your code that require resources that are not always available or if you want to test how functions interact."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"excs\"></a>\n",
    "## Exercise\n",
    "\n",
    "- Modify `test_quadratic_function_close_to` to make the test pass (consult the [&rarr; `assertAlmostEqual()` documentation](https://docs.python.org/3/library/unittest.html?highlight=unittest#unittest.TestCase.assertAlmostEqual))\n",
    "- Fix the bug that causes `test_linear_function_x_reversed` to fail\n",
    "- Manipulate `integrate_trapezoidal()` so that it fails with a `ValueError` and use the [&rarr; `assertRaises()` method](https://docs.python.org/3/library/unittest.html?highlight=unittest#unittest.TestCase.assertRaises) to verify that the exception is in fact raised"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Tip**: The Python debugger (pdb) can help you find problems with your code. Throw in a \n",
    "\n",
    "```python\n",
    "breakpoint()\n",
    "```\n",
    "\n",
    "anywhere in your code to generate a breakpoint. When running the code, the Python interpreter will stop at the breakpoint and open an interactive REPL session, which you can use to inspect variables."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: plotting_1.ipynb](plotting_1.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
