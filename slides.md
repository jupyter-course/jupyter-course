---
author: Nils Luettschwager
title: Data Evaluation with Python
subtitle: Tools and Good Practice
institute: Georg-August-University Göttingen, Institute of Physical Chemistry

date: 2022-10-17 to 2022-10-20

theme: metropolis
aspectratio: 169
toc: true
...

The course is structured as follows:

* Overture (Day 1):
  * [ ] Organizational note
  * [ ] Why a programming language instead of Excel/Origin/... ?
  * [ ] Using Jupyter (Notebooks, Terminal)
* Part I: software development tools (Day 1 and 2)
  * [ ] Environments
  * [ ] Version Control
  * [ ] Style
  * [ ] Unit tests
* [ ] Homework: build a reproducible project (own environment, version control, tests)
* Part II: scientific python libraries (Day 3)
  * [ ] Pandas
  * [ ] HoloViews

# Overture

## Organizational Note

* This methods course is part of the PC internship, module M.Che.1321, and
  weighted with 0.5 Credits (see
  [https://hbond.uni-goettingen.de/ma_chem.html](https://hbond.uni-goettingen.de/ma_chem.html))
* Enrol in FlexNow (M.Che.1321.Mk), otherwise your participation cannot be
  formally acknowledged

---

## Why a programming language instead of Excel/Origin/... ?

My comment on why I think doing our data evaluation with Python (or a programming language in general) is a good idea.

Discussion of the [sample project](https://gitlab.gwdg.de/jupyter-course/sample-project).

---

## Using Jupyter

Some skills we need to master when using Jupyter:

* Basic user interface overview
* Basic notebook editing and navigation
* What are kernels?
* Caveat: out-of-order execution
* Using the terminal
* Importing/exporting files

---

