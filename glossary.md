**Note:** The "extra nerd" (🤓) marks topics that are just nice to know, but not really required to follow the course.

The arrow (&rightarrow;) marks links to external sources, mostly Wikipedia.

Of course, the [&rarr;offical Python.org glossary](https://docs.python.org/3/glossary.html) is also a great resource.

# API

Application programming interface: roughly the way you "talk" to a software library. For example, Matplotlib's pyplot API is a specific way to create figures with this library, but not the only way, there is also an [&rarr; object-oriented API](https://matplotlib.org/stable/api/index.html).

# Argument splatting

See [course-notes/argument-splatting.ipynb](course-notes/argument-splatting.ipynb).

# Class (inheritance)

See [course-notes/class-inheritance.ipynb](course-notes/class-inheritance.ipynb).

# Decorator

See [course-notes/decorators.ipynb](course-notes/decorators.ipynb).

# DRY principle

[&rarr; Do not repeat yourself](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself): this principle states that redundancy in code should be avoided. If you feel the need to copy and paste code (more than once), consider wrapping that code in a function or class. Correcting highly redundant code is error-prone, because it is easy to overlook instances of the same code that need fixing.

# Edge case

Specific inputs to a function that need special treatment. For example: a function that performs a division by some input variable `x` must consider the edge case `x = 0`.

# Hash 🤓

A hash is a unique value (often hexadecimal) that is calculated with a specific, deterministic algorithm from a file or object in memory (any sequence of bits). The algorithm is designed such that the value completely changes when only a single bit in the original sequence changes. Therefore, one can assure that a file is correct to every single bit by comparing its hash value to a reference. When downloading files, you may have come accross MD5 hashes, which are placed on the website for you to check that you downloaded the file that you really wanted (and not a file that may have been infected by someone intercepting your communication with the server).

# Jupyter

Jupyter is a browser based interface to Python and other programming languages.
In Jupyter, you write code into *code cells* and rich text into *[markdown](#Markdown) cells* to combine data evaluation and interactive visualization in a single document. Notebooks are stored in the [&rarr; JSON](https://en.wikipedia.org/wiki/JSON) data format (🤓) and can easily be converted into standalone html or LaTeX compiled PDF documents for sharing.

Besides **Ju**lia, **Pyt**hon and **R**, many more popular programming languages can be used.

# Kernel

The kernel is a (Python) interpreter session that is run by Jupyter in the background. When you work with Jupyter notebooks, Jupyter takes your input from the code cells and sends it to the kernel for execution. While the kernel is busy executing, you cannot evaluate other code cells (they will be queued for execution if you try to run them). After execution finishes, Jupyter receives the output from the kernel and places it in the notebook.

You can have many different kernels from different Python environments (potentially running at the same time) plus kernels from other languages like R or Julia.

# Linter

Lint: a fluffy collection of dust or fibers.

[&rarr; Linters](https://en.wikipedia.org/wiki/Lint_(software)) analyze source code and can provide helpful hints about formatting, unused code, redundant code or plain syntax errors. They remove "lint" from your code.

# Local and global variables

Variables in general have a scope, which means that they only exist and may only be referred to in a specific context. A *global* variable exists in the global context of a script (or module, or notebook, or REPL session) and may be used anywhere in the code. For example, if the variable `a` is a global variable, we may use it in a function definition (though we [should not](style_3.ipynb#State,-pure-functions,-testability)):

```python
a = 1

def add_a(b):
    return a + b
```

On the other hand, a variable that was defined inside a function definition, a *local* variable, only exists while the function executes and is not available outside the *local scope* of the function:

```python
def add_x(y):
    x = 1
    return x + y

print(x) # will throw a NameError
```


# Markdown

Markdown is a simple markup language for writing formatted text using a very simple syntax.
Double-click this cell to show the raw Markdown code that produces the text below:

> ## Markdown
> With [&rarr; Markdown](https://en.wikipedia.org/wiki/mardkown), you can easily format text *italic*, **bold** or ***both***. Headings can be created using the `#` character
>
> [&rarr; MathJax 🤓](https://en.wikipedia.org/wiki/mathjax) takes care of rendering LaTeX equations:
>
> $$ y = m x + b $$

Further formatting examples can be found [&rarr; here](https://help.github.com/en/articles/basic-writing-and-formatting-syntax).

# Namespace

A name that is used to refer to a specific location of a variable.
For example, if you want to use `numpy`, it is common practice to import it like so:

```python
import numpy as np
```

Now you can access all of `numpy`'s basic functionality by typing `np.` (mind the dot) plus whatever function or class you want to use. `numpy`'s functions now "live" in the namespace `np`.

If you create an instance of an object (which you do really all the time, even when you assign `a = 1`), the object will have stuff attached to it, like methods (= functions) and other objects. You access these methods and *instance variables* by placing a dot (`.`) after your object's variable name and typing the name of the method or instance variable (nested object). For example:

```python
creator = "Guido"
creator.upper() # returns "GUIDO"
```

Here we accessed the method `upper()` through the namespace of the variable `creator`.

Final example:

```python
import os

os.listdir(os.path.curdir) # returns list of names of all files and folders
                           # in current directory
```

Here we accessed the `listdir()` function through the namespace of the module `os`, and the `curdir` instance variable through the namespace of the submodule `path`, which in turn lives in the namespace of `os`.

We also could import both, `listdir()` and `curdir`, in our *global namespace*, making them directly accessible:

```python
from os import listdir
from os.path import curdir

listdir(curdir)
```

# NodeJS 🤓

[&rarr; NodeJS](https://en.wikipedia.org/wiki/Node.js) (see also [&rarr; here](https://nodejs.org/en/about/)) is a JavaScript runtime environment. It allows you to run JavaScript outside of a browser. Much like Python, it comes with a REPL (see below) which allows you to tinker with JavaScript code on the command line. As a web application, Jupyter is build with JavaScript. Some functionality, like extensions, only work if NodeJS is installed.

# PATH Variable 🤓

Your operating system uses this variable to store a list of paths where it
searches for executables (programs). For example, when you type `python` on the
command line, the operating system is going to look through each path in that
list and runs the first executable named `python` it can find. Entries that
come first in the list have precedence, so a PATH variable like this

```bash
/home/me/my-virtual-env/bin:/home/me/.local/bin:/usr/bin
```

will instruct your OS to run `python` from the path
`/home/me/my-virtual-env/bin`, if it is available there. If not, it will look
in the next directory, `/home/me/.local/bin` and after that in `/usr/bin`
(which is a standard location for programs in Linux).

# REPL

Read-Eval-Print-Loop, the interpreter session that you use to execute programming commands interactively (e.g. by starting `python` from the terminal).

# Slug 🤓

Human readable identifier. On [&rarr; gitlab.gwdg.de](https://gitlab.gwdg.de), the project slug is used to identify your project. It is derived from your project's name by replacing spaces with dashes, removing special characters and converting all letters to small caps. See also [&rarr; this Wikipedia entry](https://en.wikipedia.org/wiki/Clean_URL#Slug).

<a id="pep"></a>

# Python Enhancement Proposal (PEP) 🤓

A [&rarr; PEP](https://www.python.org/dev/peps/) is a document that is used to communicate with the Python community. It may inform about certain guidelines, processes, or policies. Python's programming style guide is a PEP, [&rarr; PEP-8](https://www.python.org/dev/peps/pep-0008/).

