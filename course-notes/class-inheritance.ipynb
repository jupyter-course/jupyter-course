{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Class inheritance\n",
    "\n",
    "2019-11-28"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the [unittest notebook](unittest.ipynb) notebook, we came accross class inheritance: our test cases needed to be defined as subclasses of the `unittest.TestCase` class. In this notebook, I define two classes to illuminate the concept of class inheritance a little bit."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First of all: what is a class?\n",
    "\n",
    "A class is a blueprint that defines how a particular object, an *instance* of that class, can be created.\n",
    "Such objects combine data/state and methods that manipulate that data/state into a single, logical unit."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Imagine you would program a game where the player would need to be able to interact with objects in the world. If one such object happens to be a door, you would need to keep track of the door's state (open or closed) and you would need to be able to manipulate that state (open or close the door). Here is how that may translate into a Python class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Door:\n",
    "    \n",
    "    def __init__(self, id):\n",
    "        # __init__ runs when we create a new object\n",
    "        # by invoking `Door(id)`; `Door(id)` creates a new\n",
    "        # instance of the class `Door` with the identification\n",
    "        # number `id` (we want to have an id to be able to\n",
    "        # tell several doors appart from each other).\n",
    "\n",
    "        self.id = id\n",
    "        # Initially, every door is closed:\n",
    "        self.state = \"closed\"\n",
    "        \n",
    "    def print_state(self):\n",
    "        print(f\"Door with id {self.id} is {self.state}.\")\n",
    "    \n",
    "    def get_id(self):\n",
    "        return self.id\n",
    "    \n",
    "    def open(self):\n",
    "        if self.state == \"open\":\n",
    "            print(f\"Door with id {self.id} is already open!\")\n",
    "            pass\n",
    "        else:\n",
    "            self.state = \"open\"\n",
    "            print(f\"Door with id {self.id} is now open.\")\n",
    "    \n",
    "    def close(self):\n",
    "        if self.state == \"closed\":\n",
    "            print(f\"Door with id {self.id} is already closed!\")\n",
    "        else:\n",
    "            self.state = \"closed\"\n",
    "            print(f\"Door with id {self.id} is now closed.\")\n",
    "            "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We create a new door *instance* by *calling* (putting arguments in parenthesis after) the door class like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_door = Door(1)  # the argument 1 is the `id` we give to this door"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`my_door` is an instance of the class `Door`, not the class itself:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_door == Door"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(my_door) == Door"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the class definition, we defined methods (`open()`, `close()`) to manipulate our door. They are available to each door instance and manipulate only that particular instance. Watch:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_door = Door(1)\n",
    "my_door_2 = Door(2)\n",
    "\n",
    "my_door.print_state()\n",
    "my_door_2.print_state()\n",
    "my_door.open()\n",
    "my_door_2.print_state()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_door.print_state()\n",
    "my_door_2.print_state()\n",
    "my_door_2.open()\n",
    "my_door.print_state()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The door instances keep track of their internal state. A door \"knows\" that it cannot be opened again, if it is already open:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_door.print_state()\n",
    "my_door.open()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the class definition, the instance of the class object is refered to as the variable `self`. The `state` attribute of each class instance (`self.state` in the class definition) is used to keep track of the doors state and is set by the `open()` and `close()` methods according to the current state."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a simple class that models (some aspects of) a person:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Person:\n",
    "\n",
    "    def __init__(self, first_name, last_name):\n",
    "        self.first_name = first_name\n",
    "        self.last_name = last_name\n",
    "\n",
    "    def __repr__(self):\n",
    "        return self.get_full_name()\n",
    "\n",
    "    def get_full_name(self):\n",
    "        return f\"{self.first_name} {self.last_name}\"\n",
    "\n",
    "    def greet(self, other):\n",
    "        print(f\"{self} says hi to {other}!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A `Person` has a first name and a last name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "alice = Person(\"Alice\", \"Taylor\")\n",
    "alice"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "alice.first_name"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "alice.last_name"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A person can greet another person:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bob = Person(\"Bob\", \"Davies\")\n",
    "bob"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "alice.greet(bob)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we make a new class `Citizen` which can do a little bit more. We *inherit* the naming and greeting from the `Person` class and *extend* this *base class* by the possibility to vote for other citizens and keeping track of the votes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Citizen(Person):\n",
    "\n",
    "    def __init__(self, first_name, last_name):\n",
    "        super().__init__(first_name, last_name)\n",
    "        self.votes = 0\n",
    "        self.voted = False\n",
    "\n",
    "    def vote(self, other):\n",
    "        if type(other) != self.__class__:\n",
    "            print(\"You can only vote for another Citizen!\")\n",
    "            return\n",
    "        if not self.voted:\n",
    "            print(f\"{self} voted for {other}!\")\n",
    "            other.votes += 1\n",
    "            self.voted = True\n",
    "        else:\n",
    "            print(f\"{self} already voted!\")\n",
    "\n",
    "    def show_votes(self):\n",
    "        print(f\"{self} has {self.votes} votes.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create a few citizens:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mary = Citizen(\"Mary\", \"White\")\n",
    "alex = Citizen(\"Alex\", \"Hughes\")\n",
    "bishop = Citizen(\"Bishop\", \"Hall\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like a person, a citizen can greet another citizen ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mary.greet(bishop)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... or another person:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "alex.greet(bob)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `.greet()` method was inherited from the base class `Person`. To declare this inharitance, all we had to do was to put `(Person)` after the `Citizen` class definition:\n",
    "\n",
    "```python\n",
    "class Citizen(Person):\n",
    "    ...\n",
    "```\n",
    "\n",
    "as opposed to:\n",
    "\n",
    "```python\n",
    "class Citizen:\n",
    "    ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We added some extra functionality to the `Citizen` class. A citizen can vote:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bishop.vote(mary)\n",
    "alex.vote(mary)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A citizen may only vote once, though:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "alex.vote(bishop)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And a citizen may only vote for another citizen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mary.vote(alice)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mary.vote(bishop)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also defined a method to display the votes, `.show_votes()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mary.show_votes()\n",
    "bishop.show_votes()\n",
    "alex.show_votes()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By using class inheritance, we avoided duplicating the naming/greeting code.\n",
    "Without inheritance we would have needed to do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Citizen:  # mind the missing \"(Person)\"\n",
    "\n",
    "    def __init__(self, first_name, last_name):\n",
    "        self.first_name = first_name\n",
    "        self.last_name = last_name\n",
    "        self.votes = 0\n",
    "        self.voted = False\n",
    "\n",
    "    def __repr__(self):\n",
    "        return self.get_full_name()\n",
    "\n",
    "    def get_full_name(self):\n",
    "        return f\"{self.first_name} {self.last_name}\"\n",
    "\n",
    "    def greet(self, other):\n",
    "        print(f\"{self} says hi to {other}!\")\n",
    "\n",
    "    def vote(self, other):\n",
    "        if type(other) != self.__class__:\n",
    "            print(\"You can only vote for another Citizen!\")\n",
    "            return\n",
    "        if not self.voted:\n",
    "            print(f\"{self} voted for {other}!\")\n",
    "            other.votes += 1\n",
    "            self.voted = True\n",
    "        else:\n",
    "            print(f\"{self} already voted!\")\n",
    "\n",
    "    def show_votes(self):\n",
    "        print(f\"{self} has {self.votes} votes.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "to get the same functionality:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mary = Citizen(\"Mary\", \"White\")\n",
    "alex = Citizen(\"Alex\", \"Hughes\")\n",
    "bishop = Citizen(\"Bishop\", \"Hall\")\n",
    "\n",
    "mary.greet(bishop)\n",
    "alex.greet(bob)\n",
    "\n",
    "bishop.vote(mary)\n",
    "alex.vote(mary)\n",
    "alex.vote(bishop)\n",
    "mary.vote(alice)\n",
    "mary.vote(bishop)\n",
    "\n",
    "mary.show_votes()\n",
    "bishop.show_votes()\n",
    "alex.show_votes()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
