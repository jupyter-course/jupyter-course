import nbformat
import os
import unittest

from nbconvert.preprocessors import ExecutePreprocessor

PROJ_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
KERNEL = "jupyter-course"


def run_notebook(notebook_path):
    nb = nbformat.read(notebook_path, as_version=4)
    ep = ExecutePreprocessor(kernel_name=KERNEL)
    # metadate declares _where_ to execute notebook
    ep.preprocess(nb, {"metadata": {"path": PROJ_DIR}})


class TestEvaluationNotebooks(unittest.TestCase):
    def _test_notebook_runs_wo_error(self, notebook_path):
        run_notebook(notebook_path)

    def test_executes_wo_error_evaluation(self):
        for fn in filter(lambda x: x.endswith(".ipynb"), os.listdir(PROJ_DIR)):
            if "unittest" in fn:
                continue
            print(f"Running {fn}")
            fp = os.path.join(PROJ_DIR, fn)
            with self.subTest(msg=f"running {fn}"):
                self._test_notebook_runs_wo_error(fp)


if __name__ == "__main__":
    print(f"Running notebooks using kernel '{KERNEL}'")
    unittest.main()
