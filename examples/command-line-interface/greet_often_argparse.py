"""
greet_argparse.py

A simple demonstration of parsing arguments from the
command line to Python using argparse.
"""

import argparse


def get_args():
    # formatter_class = argparse.RawDescriptionHelpFormatter avoids
    # automatic line wrapping in the description
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('first_name', type=str, help='your first name')
    parser.add_argument('last_name', type=str, help='your last name')
    parser.add_argument('-r', '--repetitions', type=int, default=1,
                        help='how often too greet')
    parser.add_argument('-f', '--friendly', action='store_true', default=False)
    return parser.parse_args()


def main():
    args = get_args()
    greeting = f"Hello, {args.first_name} {args.last_name}"
    greeting += "! How are you?" if args.friendly else "."
    
    for _ in range(args.repetitions):
        print(greeting)

if __name__ == '__main__':
    main()
