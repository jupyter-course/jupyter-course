{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false
   },
   "source": [
    "# Coding style (II)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... continues [style_1.ipynb](style_1.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"tblcts\">*Table of contents*</a>\n",
    "\n",
    "- [Naming things](#Naming-things)\n",
    "  - [Variables](#Variables)\n",
    "  - [Functions](#Functions)\n",
    "  - [Classes](#Classes)\n",
    "  - [Caveat](#Caveat)\n",
    "- [Documentation](#Documentation)\n",
    "  - [Docstrings](#Docstrings)\n",
    "    - [PEP 257 & Co](#PEP-257-&-Co)\n",
    "    - [Examples](#Examples)\n",
    "  - [Comments](#Comments)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Naming things"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Choosing good names for your variables, functions, and classes is a way of documenting your code in a [DRY](glossary.md#DRY-principle) way and makes it easy (or at least easier) to read and understand it.\n",
    "A good name should be informative, i.e. tell what something *is* (variables/classes), what something *does* (functions/methods) or how something *behaves* (classes). You should avoid using very generic names, mere placeholders (like `foo`, `bar`, `baz`), uncommon or ambiguous abbreviations, names which collide with keywords reserved by Python or very similar names which invite using accidentially the wrong one. Using names which are not pronouncable will make you \"trip\" over them when reading the code.\n",
    "\n",
    "I think it is helpful to look at many examples and see the problems with certain names for yourself, so this is what we are going to do."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variables\n",
    "\n",
    "Good variable names tell you what something is and what it is used for, so a noun or noun phrase and a preceding adjective is often useful.\n",
    "\n",
    "Compare these variable names:\n",
    "\n",
    "1. `foo`\n",
    "2. `kw`\n",
    "3. `kwargs`\n",
    "4. `fig_kwargs`\n",
    "5. `default_figure_kwargs`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first name is just a \"non-name\", it does not contain any information (other than the programmer was being lazy). The second name is slightly better. At least it tries to somehow encode information about the content of the variable, but it is clearly short and ambiguous. The third name, `kwargs`, is a typical abbreviation used in Python for *keyword arguments*. It tells us that this variable will probably be passed as keyword arguments to some function. But what function? The forth name includes a *fig* in the name. This is often used for *figure* and in the context of the code (and in a limited scope, see below) this may be fine. Still, the fifth name is much better. Additionally of telling us what the variable represents, it also tells us something about its intended purpose."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is another example:\n",
    "\n",
    "1. `n`\n",
    "2. `n_iter`\n",
    "3. `max_iter`\n",
    "4. `max_iterations`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I would not say that `n_iter` is a particularly bad name, given that `n` is often used to indicate an amount and `iter` is a common abbreviation for *iterations*, but I would argue that using *max* to indicate that this variable is intended to be an upper bound (and that the particular loop not neccessarily runs for *exactly* `n` iterations) is helpful and the third and forth version is less ambiguous."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Integrating some information about the inteded purpose of a variable may thus be helpful, but using very long names everywhere will also make the code look cluttered and less readable. You do not need to give a very long name to a variable that appears only locally within a very small piece of code (e.g. a local variable in a short function)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Function arguments (including keyword arguments) deserve special care, because they are used very often and help you to recall how a certain function is used and what it does. If you have written a function that is used in many places, perhaps accross several projects, changing the name of the arguments later will break compatibility."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions are supposed to *do* something, so using a verb in the function's name can often be helpful to understand what a particular function does. A well named function can tell you what it does without you having to look at its documentation or its implementation (its code, the function body)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some functions defined in `pandas/io/common.py`:\n",
    "\n",
    "> ```python\n",
    "> is_url(...)\n",
    ">\n",
    "> validate_header_arg(...)\n",
    "> \n",
    "> stringify_path(...)\n",
    "> ```\n",
    "\n",
    "\n",
    "(here and in the following, I removed some preceding underscores for clarity)\n",
    "\n",
    "Can you guess from the function names what the particular function is about? What will the function do? What may be its return type?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare to the following functions that I pulled from `bokeh/plotting/helpers.py`:\n",
    "\n",
    "> ```python\n",
    "> single_stack(...)\n",
    ">\n",
    "> double_stack(...)\n",
    ">\n",
    "> graph(...)\n",
    "> ```\n",
    "\n",
    "How about now? What do these functions do?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare these function names ...\n",
    "\n",
    "`scipy/optimize/_lsq/common.py`\n",
    "\n",
    "> ```python\n",
    "> in_bounds(...)\n",
    ">\n",
    "> step_size_to_bound(...)\n",
    ">\n",
    "> solve_trust_region_2d(...)\n",
    "> ```\n",
    "\n",
    "... to these function names:\n",
    "\n",
    "`scipy/odr/models.py`\n",
    "\n",
    "> ```python'\n",
    "> exp_est(...)\n",
    "> \n",
    "> exp_fjb(...)\n",
    "> \n",
    "> exp_fjd(...)\n",
    "> ```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's try to find out what makes some of these functions easier to understand than others.\n",
    "\n",
    "**What you can note from the examples is that there seem to be certain words which signal a specific behavior.**\n",
    "\n",
    "The verb ***is*** signals that we want to check that some condition is true of false. We would expect a function named like that to return a boolean, i.e. `True` or `False`. If this is the case, we can use such a function in an if-statement like so:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "if is_url(some_string):\n",
    "    # do something with the url `some_string`\n",
    "else:\n",
    "    # do something else with the string `some_string`\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note how the *is* in the function name not only tells us something about the function but how it also helps to make this code particularly readable (the wording is close to a regular english sentence)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, the preposition ***in*** signals that we want to check whether a value falls in a certain range or is a member of some collection. Again, an if-statement using a function which does something like this would be well readable:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "if in_bounds(some_value):\n",
    "    # do something with the value\n",
    "else:\n",
    "    # do something else with the value\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ***validate*** in `validate_header_arg(...)` suggests that we want to check whether or not we can use some object in a specific context or that we want to enforce a specific data type or structure. A validation can either pass or fail. What should happen when the validation passes? Nothing, we can just proceed, the function returns `None`. If the test fails the function should make the user aware of it: it prints a warning or raises an exception (an error). This is exactly the behavior of `validate_header_arg(...)`, and we can guess this simply from the verb *validate* in the function name."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`stringify_path(...)` follows the naming pattern ***verb_object***, i.e. *do* something with the *object*. Though *stringify* seems a little stretched, it helps to make clear that the return type of the function will probably be a string, derived from some object that represents a filepath."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous example, the verb *stringify* indicated the return type of the function. Even more, the name suggested that some conversion was taking place. Another common pattern to name a function which converts some object is ***x_to_y***, as in `step_size_to_bound(...)`. Immediately we get an idea what goes in and what comes out."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Compare this to the other function name examples:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In `single_stack(...)` and `double_stack(...)` it is not clear if *stack* is a noun or a verb. What do we stack? Or: what is done to or with the single/double stack? That the function has no docstring (see below) and a lot of variables called `i`, `d`, `s` does not help to infer the functionality from the context. It is possible, but it requires much more mental lifting than neccessary."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`graph(...)` has no verb or other signaling word. Perhaps *graph* is ment as a verb? So does it produce a graph? From the context we can see that it returns a dictionary that is ment to be passed as keyword arguments to another function. We could not have guessed that from the function name. Can you come up with a better function name?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, the `exp_...` functions from `scipy/odr/models.py` use very short abbreviations. While short wording helps to make the code appear less cluttered, here we are left guessing what the abbreviations may mean. Again, it is possible to infer from context, but its harder to \"parse\" the code mentally."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To be fair, it may not always be feasible or practical to give long and declarative function names, and some function names can be mentally \"translated\" if looking at the context (this is true for the `scipy/odr/models.py` examples). Still, when reading cryptic function names, we wish a more descriptive name had been chosen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Common verbs and usage examples**\n",
    "\n",
    "So words signaling a specific behavior, like looking up something, converting something, checking something, etc. are a good basis to give function descriptive names. Even better when such names create a nice flow when reading the code, like `if is_url(some_string): ...`.\n",
    "\n",
    "Some common verbs to signal behavior are:<sup>1</sup>\n",
    "\n",
    "- *get* and *set*\n",
    "- *join*, *merge*, *append*, *concatenate*\n",
    "- *split*, *slice*\n",
    "- *save*, *store*, *write*\n",
    "\n",
    "---\n",
    "<sup>1</sup> Kathrin Passig, Johannes Jander, *Weniger schlecht programmieren*, O'Reilly, Köln, **2013**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Classes\n",
    "\n",
    "Classes are blueprints for objects which behave in a specific way. So a class will typically be named using a noun (and adjective).\n",
    "\n",
    "Typical naming patterns are (examples are from the Python standard library):\n",
    "\n",
    "- Objects (e.g. `TemporaryFile` from `tempfile`, `Template` from `string`)\n",
    "- \"Do-ers\" (e.g. `DictReader` and `DictWriter` from `csv`, `Counter` from `collections`)\n",
    "- \"Is-ers\" (the name focusses on particular property, e.g. `Hashable` and `Iterable` from `collections.abc`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "General names like `Manager` or `Processor` ar controversial but since classes are often derived from other classes through inheritance, naturally some classes, the base classes, will be quite general. A possible solution is to explicitly include the `Base` in the name of a base class, as in `BaseReader` from which you than may derive a `CSVReader` and a `JSONReader`. In this particular example, it would also be helpful to include a `File` in the name, to make clear that we intend to read files (and do not want to parse strings, for example): `BaseFileReader`, `CSVFileReader`, `JSONFileReader`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Caveat\n",
    "\n",
    "Even though good naming helps you to understand code more intuitively, you should not rely on your intuition alone. When I debug my code, quite often I find errors which are based on my assumptions about how some function or object behaves. You (and I) should thus double check by reading the code documentation. Which brings us to our next topic."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Documentation\n",
    "\n",
    "If you want to be able to understand your code in a few weeks from now or you want other people to be able to understand and use your code, you have to write documentation.\n",
    "\n",
    "There are several places where the parts of your documentation live:\n",
    "\n",
    "- A README file which comes with your project or package\n",
    "- A docstring at the beginning of a module (a .py file)\n",
    "- A docstring at the first line of a function/class definition\n",
    "- Inline comments which address specific parts of the code"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Docstrings\n",
    "\n",
    "A [&rarr; docstring](https://www.python.org/dev/peps/pep-0257/#what-is-a-docstring), as the name suggests, is a string that contains documentation. In Python you can add docstrings simply by adding a string as the first statement in a module, class, method, or function definition.\n",
    "To add a docstring to a function, for example, you include it directly after the `def` statement. The following code cell shows a generic example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_function(a, b):\n",
    "    \"\"\"<One-line description what function does>\n",
    "\n",
    "    <Additional information and/or notes/hints>\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    a: <type(s)>\n",
    "        <description of parameter a>\n",
    "    b: <type(s)>\n",
    "        <description of parameter b>\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    c: <type>\n",
    "        <description of output c>\n",
    "\n",
    "    Example(s) [optional]\n",
    "    ----------\n",
    "\n",
    "    <code examples>\n",
    "    \"\"\"\n",
    "\n",
    "    c = a + b\n",
    "    return c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's test the docstring:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "my_function(1, 2)  # place cursor in parenthesis and press shift + tab"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember the module `linter_test.py`? This module also has a docstring at the beginning, albeit a very short (and not very good) one.\n",
    "\n",
    "If we ask for help using `?`, it is displayed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from examples import linter_test\n",
    "\n",
    "linter_test?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### PEP 257 & Co\n",
    "\n",
    "The basis for docstring guidelines is [&rarr; PEP 257](https://www.python.org/dev/peps/pep-0257/) which summarizes what a docstring should and should not include and how it should be formatted. Note that even the standard library of Python, probably for historic reasons, does not entirely follow this guide.\n",
    "\n",
    "Big projects may have their own documentation style guides which extend PEP 257 and give more detailed instructions. The particular format used by Numpy and Scipy is a good example. You can find detailed guidelines for these projects [&rarr; here](https://numpydoc.readthedocs.io/en/latest/format.html).\n",
    "\n",
    "Keep in mind that these projects provide code for countless users, so its documentations is of course rather comprehensive. You do not need to write pages of explanations for each and every function you define, but keep in mind that having no docstrings means that you will need to read through the implementation of a function that you want to use. This can slow you down quite a bit and in the worst case can make you rather rewrite than reuse your code. So at least, give a good descriptive name (see above) and document what arguments the functions takes and what it returns."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Examples\n",
    "\n",
    "One-liners, written using the format \"Do this and return that.\", may be okay for very simple functions. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def rescale_array(x):\n",
    "    \"\"\"Rescale a Numpy array to the interval [0, 1].\"\"\"\n",
    "    span = x.ptp()\n",
    "    return (x - x.min())/span"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import exp\n",
    "\n",
    "exp?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If more information is necessary, another comment is added to the docstring, for example in case of the `all()` function where particular behavior for an [edge case](glossary.md#Edge-case) is documented:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "all?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More complex functions need more detailed documentation. Here is an example from Numpy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "np.linspace?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Comments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While docstrings help you to understand how to use code, comments help you to understand how the code (the implementation) actually works. They are another important puzzle piece which you can use to make your code understandable to your future self.\n",
    "\n",
    "Commenting is often a good thing, but it can also be harmful, so I want to share a short list of recommendations.\n",
    "\n",
    "**Do not comment the obvious:**\n",
    "\n",
    "```python\n",
    "data = load_data(filepath)  # load the data\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the clear syntax of Python and good names, comments like this are just redundant. Reading such comments is tiresome and you are in danger of skipping and missing important comments when there are a lot of obvious ones. Also, the more comments of this kind are present, the higher is the chance that you forget to update such a comment when you update your code. Than you have something which is worse than no comment: a comment that lies to you.\n",
    "\n",
    "**Better things to address in comments are:**\n",
    "\n",
    "Your intent. What do you want the code to do?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# this part implements the algorithm ... see https:// ... doi.org/ ...\n",
    "while ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# we need to make sure that ... otherwise ...\n",
    "if ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# this may fail because ...\n",
    "try: ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your decisions. Why did you include a specific action?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# we can center the data around zero to improve numerical stability\n",
    "data -= data.mean()  \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# the following calculations require at least double precision\n",
    "x = x.astype(np.float64)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# This looks convoluted, but the simpler implementation:\n",
    "# \n",
    "# <other implementation>\n",
    "#\n",
    "# is much slower, perhaps because ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your problems. Is there room for improvement?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# TODO: for some reason, this fails at midnight ...\n",
    "if datetime.now():\n",
    "    ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# TODO: I don't know why the next step is neccessary,\n",
    "# looks like a bug in some_module (drops the last item?)\n",
    "# Could be me, though ...\n",
    "items += [0]\n",
    "some_module.do_something(items)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "# TODO: I hard-coded this tolerance based on experience.\n",
    "# Maybe there is a better way to infer a good value from the data?\n",
    "tolerance = 4e-8\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try to formulate your comments as precisely as possible. As with comments that contradict the code, comments that are confusing or ambiguous (careful with abbreviations!) are not helpful."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To end this section, here is a list with \"the best\" comments that programmers ever wrote (according to [&rarr; stackoverflow](https://stackoverflow.com/questions/184618/what-is-the-best-comment-in-source-code-you-have-ever-encountered)):\n",
    "\n",
    "- ```\n",
    "  // When I wrote this, only God and I understood what I was doing\n",
    "  // Now, God only knows\n",
    "  ```\n",
    "\n",
    "\n",
    "- ```\n",
    "  stop(); // Hammertime!\n",
    "  ```\n",
    "\n",
    "\n",
    "- ```\n",
    "  // I dedicate all this code, all my work, to my wife, Darlene, who will \n",
    "  // have to support me and our three children and the dog once it gets \n",
    "  // released into the public.\n",
    "  ```\n",
    "\n",
    "\n",
    "- ```\n",
    "  // Magic. Do not touch.\n",
    "  ```\n",
    "\n",
    "\n",
    "- ```\n",
    "  // I'm sorry.\n",
    "  ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tblcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: style_3.ipynb](style_3.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
