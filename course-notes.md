# Special Methods Course: Software Development Tools and Reproducible Data Evaluation

2024-02-13

## Useful Links

To access this document, go here:

<div style="margin: 1em; padding-left: 0.5em; border-left: 6px solid #ccccff">
    <a style="font-size:20pt"
       href="https://s.gwdg.de/s63hGh">
    https://s.gwdg.de/s63hGh
    </a>
</div>


To access the course material *interactively*, follow [this link](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fjupyter-course%2Fjupyter-course/feb24) to mybinder.org.

The source files of the course are available here:

https://gitlab.gwdg.de/jupyter-course/jupyter-course


Access the Jupyter Cloud service from GWDG:

https://jupyter-cloud.gwdg.de

## Seminar 1 --- Jupyter and Environments

### Cheat-Sheet Jupyter

`enter` to switch to **edit mode** (type text into cells)

`escape` to switch to **command mode** (move around with cursor keys, add cells, etc.)

`shift + enter` to execute a cell

In command mode:

`00` to restart kernel

`a` to make a new cell **a**bove the currently selected cell

`b` to make a new cell **b**elow the currently selected cell

`m` to set cell type to markdown

`y` to set cell type to code

`x` to cut a cell

`v` to paste a cell (below selected cell)

`dd` to delete a cell

`z` to undo a cell operation (like delete)

`shift + z` to redo a cell

`shift + ctrl + c` (edit or command mode) to open command menu → see keyboard shortcuts on the right hand side

![keyboard shortcuts in command menu](https://pad.gwdg.de/uploads/c6df4845-9039-455d-a181-09bcd0d2a23a.png)

When you finised a notebook, always restart the kernel and run it from the top, to avoid errors due to *out of order execution*. Open command menu with `shift + ctrl + c` and type "rerun", which should select the option "Restart Kernel and Run All Cells...", and run this command by pressing `enter`.

![restart kernel and run all in command menu](https://pad.gwdg.de/uploads/272e7319-57e1-4def-aece-89b4ae660933.png)


### Cheat-Sheet Environments

Create environment:

```bash
$ python -m venv myenvironment --copies
```

Activate the environment:

```bash
$ source myenvironment/bin/activate
```

Install `ipykernel` (package that enables your environment to talk to jupyter):

```bash
(myenvironment) $ pip install ipykernel
```

Install a kernel specification, so that jupyter knows about your environment:

```bash
(myenvironment) $ python -m ipykernel install --name myenvironment --user
```

Now you should have a new kernel available when creating a new notebook (or switch the kernel in an existing notebook).


Install further dependencies:

```bash
(myenvironment) $ pip install matplotlib scipyt pandas ...
```

Create a list of your installed packages ...

```bash
(myenvironment) $ pip freeze
```

... and redirect the output into a file:

```bash
(myenvironment) $ pip freeze > requirements.txt
```

Install dependencies from a `requirements.txt` file:

```bash
(myenvironment) $ pip install -r requirements.txt
```

## Seminar 2 --- Version Control (1) --- Git Basics

You find extensive documentation on git on the official website:

https://git-scm.com/

On first usage of git, you will have to set your name and email address:

```bash
$ git config --global user.name "Your Name"
$ git config --global user.email "you@example.com"
```

### Cheat Sheet

Initialize a new repository in an existing folder:

```bash
$ mkdir my-repo
$ cd my-repo
$ git init # this command initializes the repo (creates folder `.git`)
```

Add files to the folder and then stage them for your next commit:

```bash
$ git add my-file.txt
```

Commit your changes and provide a commit-message:

```bash
$ git commit -m "my commit message"
```

Change your file and watch changes by "diffing"

```bash
$ git diff my-file.txt
```

View a log of your commits:

```bash
$ git log
```

Amending commits (adding changes and/or changing commit message):

```bash
$ git commit --amend -m "commit message"
```

### Homework

Work through the notebook `version_control.ipynb`, excluding the sections on syncing the repo with a remote (gitlab). This will be part of the third seminar.

Go to gitlab.gwdg.de and login (this activates your GWDG account for use with gitlab).

## Seminar 3 --- Version Control (2) --- Working with Remote Repository

### Cheat Sheet

`clone` a repository (make a local copy)

```bash
$ git clone <url of repo>
```

If you use an access token to authenticate:

```bash
$ git clone https://<your username>@gitlab.gwdg.de/<your username>/<slug of your repo>
```

`push` local commits to a repository (sync changes back to remote repo)

```bash
$ git push
```

Check for changes on the remote repo that you do not yet have locally

```bash
$ git fetch
```

`pull` commits from the remote repo that you do not yet have locally

```bash
$ git pull
```


### Homework

Work through notebooks `style_1.ipynb`, `style_2.ipynb`, `style_3.ipynb`

(Optional) Evaluate the course via StudIP:

![](https://pad.gwdg.de/uploads/5fb57e69-6916-4ad9-89b3-4bf31bab623d.png)



## Seminar 4 --- Unit-Testing

Course example and basic recipe:

```python
# mymodule.py
# defines the function we want to test
def div(a, b):
    return a / b

```

Testing with Python module `unittest` (from standard library, always available):
Make a class `Test<Something>` that _inherits_ from class `unittest.TestCase`.
`unittest.TestCase` will provide you with certain assertion function, that you can
call via the instance variable `self` (e.g. `self.assertEqual(a, b)`):


```python
# test_mymodule.py
import unittest

from mymodule import div


class TestDiv(unittest.TestCase):

    def test_divide_one(self):
        numerator = 2
        denominator = 1
        self.assertEqual(
            div(numerator, denominator),
            2,
            f"this did not work: numerator was {numerator}, denominator was {denominator}",
        )

    def test_divide_zero(self):
        numerator = 1
        denominator = 0
        self.assertRaises(
            ZeroDivisionError,  # putting a different Expection here will result in an error
            div,
            numerator,
            denominator,
        )
```

Run tests with _auto-discover_ feature of module `unittest` on command line
(requires that the filename of the file containting the test code starts with `test_`):


```bash
# in folder where `mymodule.py`, `test_mymodule.py` and the file `__init__.py` is located
$ python -m unittest
```

Expected output:

```
..
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

**Note**: During the course, the test with `assertRaises()` (within `test_divide_zero`)
did not seem to work properly, because an error was thrown when one would provide another expection than the actually produced expection `ZeroDivisionError` as the first argument.
**This is the expected and correct behavior!**
See https://docs.python.org/3/library/unittest.html#unittest.TestCase.assertRaises

## Homework (after the 4th seminar)

see `homework.md` in course repository.

Try to run your project on mybinder.org. See https://gitlab.gwdg.de/nluetts/test-repo as an example. 

**Deadline: Feb. 29**

