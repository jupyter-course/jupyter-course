{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Interactive plots (I)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2019-11-11\n",
    "\n",
    "N. Luettschwager\n",
    "\n",
    "Note for March 2021 course: This notebook will only work correctly with classic Jupyter Notebook, not JupyterLab v3."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"tableofcontents\">*Table of contents*</a>\n",
    "\n",
    "- [Bokeh figures](#Bokeh-figures)\n",
    "  - [Minimal example](#Minimal-example)\n",
    "  - [Scatter plots](#Scatter-plots)\n",
    "  - [Styling Bokeh plots](#Styling-Bokeh-plots)\n",
    "    - [Figure appearance](#Figure-appearance)\n",
    "    - [Plot appearance](#Plot-appearance)\n",
    "    - [Exercise 1](#Exercise-1)\n",
    "  - [Bar charts](#Bar-charts)\n",
    "  - [Layouts](#Layouts)\n",
    "    - [Exercise 2](#Exercise-2)\n",
    "  - [Data sources and dynamic plot updates](#Data-sources-and-dynamic-plot-updates)\n",
    "    - [\"Pushing\" changes](#\"Pushing\"-changes)\n",
    "    - [Plotting with a ColumnDataSource](#Plotting-with-a-ColumnDataSource)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Perhaps one of the most important tasks of a scientist is to transform and present her data in a way that allows to make discoveries and present these discoveries in a clear way. Since this is hardly ever possible with tabular data alone, we have to prepare diagrams that encode the data in a meaningful way: we have to plot our data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Python there exist many plotting libraries to choose from:\n",
    "\n",
    "- [Altair](https://altair-viz.github.io/) ([PyCon 2018 tutorial on Youtube](https://www.youtube.com/watch?v=ms29ZPUKxbU))\n",
    "- [Bokeh](https://docs.bokeh.org/en/latest/index.html) ([PyCon 2017 tutorial on YouTube](https://www.youtube.com/watch?v=xId9B1BVusA))\n",
    "- [bqplot](https://bqplot.readthedocs.io/en/latest/) ([SciPy 2018 talk on YouTube](https://www.youtube.com/watch?v=Dmxa2Kyfzxk))\n",
    "- [HoloViews](https://holoviews.org/index.html) ([SciPy 2019 tutorial on YouTube](https://www.youtube.com/watch?v=7deGS4IPAQ0))\n",
    "- [Matplotlib](https://matplotlib.org/) ([SciPy 2019 tutorial on YouTube](https://www.youtube.com/watch?v=Tr4DYo4v5AY))\n",
    "- [Plotly](https://plot.ly/) ([PyData 2018 talk on YouTube](https://www.youtube.com/watch?v=cuTMbGjN2GQ))\n",
    "- [Seaborn](https://seaborn.pydata.org/index.html) ([PyData 2017 tutorial on YouTube](https://www.youtube.com/watch?v=KvZ2KSxlWBY))\n",
    "\n",
    "Surly, you will already have plotted some data with Matplotlib, which is perhaps the gold standard for plotting in Python.\n",
    "While Matplotlib is a great and versatile tool, newer libraries like Altair and Bokeh focus on the web browser as main visualization platform and are thus better equipped for working with a web-based programming environment like Jupyter. In this notebook, I will mainly focus on Bokeh and show how a somewhat complex visualization may be put together using this library. To demonstrate how differently one can approach the same problem, I will also show (almost) the same visualization prepared with Altair."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of the benefits of working with Jupyter is that it is relatively easy to build small graphical user interfaces using so called [*widgets*](https://ipywidgets.readthedocs.io/en/latest/). With widgets you can easily manipulate plots and explore your data in a playful way.\n",
    "You can compile much more information in a single figure and just switch between different views or portions of the data, which accelerates your analysis. I will introduce the basic mechanism of using widgets to achieve interactivity and show how they can be combined with Bokeh to build interactive dash boards."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bokeh figures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Jupyter, we typically want the plots to be displayed directly in the notebook. We have to tell Bokeh to do this explicitly (by default, the output is directed in to separate html files). You do this by calling the `output_notebook()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bokeh.plotting import output_notebook\n",
    "\n",
    "output_notebook()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Minimal example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creating a Bokeh figure requires just two basic ingredients:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bokeh.plotting import figure, show"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `figure()` function creates a figure object to which we can add plots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = figure()\n",
    "type(fig)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To add a line plot, we call the `fig.line()` method and pass the x-y data as the first two arguments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "x = np.arange(4)\n",
    "y = np.array([3.9, 6.1, 8, 9.9])\n",
    "\n",
    "fig.line(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in Bokeh, the objects that represent the actual graphs (here the line) are called [glyphs](https://docs.bokeh.org/en/latest/docs/user_guide/quickstart.html#glyphs).\n",
    "\n",
    "We display the figure using the `show()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show(fig)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On the right side of the figure you find a toolbar. By default, a pan, box zoom, mouse wheel zoom, save-to-file, reset, and help tool are displayed. Click the different tools and play with them to see how they behave. This is the most simple (and "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Scatter plots"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Besides `fig.line()`, the figure object has further methods to draw the data with circles, squares, diamonds, or other symbols:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = figure()\n",
    "\n",
    "fig.circle(x, y)\n",
    "fig.square(x, y - 0.1)\n",
    "fig.x(x, y + 0.1)\n",
    "\n",
    "show(fig)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Styling Bokeh plots\n",
    "\n",
    "Before having a look at other plot types, let us explore how we can customize our plots' appearance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Figure appearance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The look of your figure can be altered upon figure creation by passing specific [keyword arguments](https://docs.bokeh.org/en/latest/docs/reference/plotting.html#bokeh.plotting.figure.figure) to the `figure()` function.\n",
    "Here are some keywords that I use rather often:\n",
    "\n",
    "- `title` (`str`): the title of the figure\n",
    "- `width`/`height` (`int`): figure width and height\n",
    "- `x_axis_label`/`y_axis_label` (`str`): labels for x- and y-axis\n",
    "- `x_range`/`y_range` (`tuple(float, float)`): data range for x- and y-axis\n",
    "- `x_axis_type`/`y_axis_type` (`str`): axis type, e.g. `log`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = figure(width=600,\n",
    "             height=300,\n",
    "             title=\"My Bokeh figure\",\n",
    "             x_axis_label='my x axis label',\n",
    "             y_axis_label='my y axis label',\n",
    "             x_range=(-2, 5),\n",
    "             y_range=(1, 1000),\n",
    "             y_axis_type='log')\n",
    "\n",
    "fig.line(x, y)\n",
    "fig.circle(x, y)\n",
    "\n",
    "show(fig)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More fine grained control is possible by manipulating the figure object's attributes directly. For example, the font size of the labels can be set after figure creation like so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig.title.text_font_size = '14pt'\n",
    "fig.xaxis.axis_label_text_font_size = '14pt'\n",
    "fig.yaxis.axis_label_text_font_size = '14pt'\n",
    "fig.xaxis.major_label_text_font_size = '12pt'\n",
    "fig.yaxis.major_label_text_font_size = '12pt'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show(fig)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot appearance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To manipulate how the actual plot looks like, we can pass specific keywords to the figure methods that add glyphs. For example:\n",
    "\n",
    "- `color` (`str`): the color of the plot (can also be a Bokeh color object)\n",
    "- `size` (`int`): the size of the plot's markers (marker plot only)\n",
    "- `line_width` (`int`): line width of line plots or of lines around markers\n",
    "- `legend_label` (`str`): the plot's legend\n",
    "- `alpha` (`float` between 0 and 1): opacity of the plot\n",
    "\n",
    "`color` and `size` can also encode the data. Then we would not pass a simple string or integer. We will come back to this in a bit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = figure(width=600, height=300, x_range=(-0.2, 4.2))\n",
    "\n",
    "fig.line(x, y, color=\"red\", line_width=3)\n",
    "# the color can also be define using a RGB value in hexadecimal notation\n",
    "fig.circle(x, y, color=\"#ff0000\", size=15, legend_label=\"a red plot\")\n",
    "\n",
    "show(fig)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot this data ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(42)\n",
    "\n",
    "x1 = np.random.randn(1000) - 0.5\n",
    "y1 = np.random.randn(1000) - 0.5\n",
    "\n",
    "x2 = np.random.randn(1000) + 0.5\n",
    "y2 = np.random.randn(1000) + 0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... into a single figure so that the centers of both point clouds are well visible."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# your plot:\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Bar charts\n",
    "\n",
    "A bar chart can be created by using the `fig.vbar()` method (`v` = vertical). For example, to plot a histogram, we can do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "counts, bin_edges = np.histogram(x1)\n",
    "bin_centers = (bin_edges[:-1] + bin_edges[1:])/2\n",
    "bar_width = bin_centers.ptp()/10\n",
    "\n",
    "fig_vbar = figure(height=300, width=600, title=\"histogram of x1\")\n",
    "\n",
    "fig_vbar.vbar(bin_centers, bar_width, counts)\n",
    "\n",
    "show(fig_vbar)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `fig.hbar()` method add a horizontal bar chart:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "counts, bin_edges = np.histogram(y1)\n",
    "bin_centers = (bin_edges[:-1] + bin_edges[1:])/2\n",
    "bar_width = bin_centers.ptp()/10\n",
    "\n",
    "fig_hbar = figure(width=300, height=600, title=\"histogram of y1\")\n",
    "\n",
    "fig_hbar.hbar(bin_centers, bar_width, counts)\n",
    "\n",
    "show(fig_hbar)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Layouts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Figures can be combined into a single figure using *layouts*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bokeh.layouts import column, gridplot, row"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lining up figures horizontally is done with the `row()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_left = figure(height=300, width=300, title=\"left\")\n",
    "fig_right = figure(height=300, width=300, title=\"right\")\n",
    "\n",
    "fig_left.circle(x1, y1, legend_label=\"y1 vs. x1\")\n",
    "fig_right.circle(x2, y2, legend_label=\"y2 vs. x2\", color=\"red\")\n",
    "\n",
    "show(row(fig_left, fig_right))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stacking figures vertically is done with the `column()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_top = figure(height=300, width=300, title=\"top\")\n",
    "fig_bottom = figure(height=300, width=300, title=\"bottom\")\n",
    "\n",
    "fig_top.square(x1, x2, legend_label=\"x2 vs. x1\", color=\"green\")\n",
    "fig_bottom.square(y1, y2, legend_label=\"y2 vs. y1\", color=\"violet\")\n",
    "\n",
    "show(column(fig_top, fig_bottom))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`row()` and `column()` can be combined:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show(row(column(fig_left, fig_bottom), column(fig_top, fig_right)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the compiled figure looks somewhat crowded, due to the four toolbars.\n",
    "We can used `gridplot()` to make the figures share one toolbar. Note that you have to pass the figures in a nested list (list of lists) to `gridplot()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show(gridplot([[fig_left, fig_top], [fig_bottom, fig_right]]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exercise 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using `fig_hbar`, `fig_vbar`, `fig_left`, and `gridplot()`, create the figure below.\n",
    "\n",
    "**Tip:** To leave one spot in the gridplot empty, pass `None` instead of a figure instance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![figures/bokeh-scatter-histograms.png](figures/bokeh-scatter-histograms.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Data sources and dynamic plot updates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(we make a new figure to introduce data source objects)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_ds = figure(height=300, width=300, x_range=(-5, 5), y_range=(-5, 5))\n",
    "\n",
    "np.random.seed(42)\n",
    "\n",
    "fig_ds.circle(np.random.randn(20), np.random.randn(20))\n",
    "fig_ds.circle(np.random.randn(20), np.random.randn(20), color='red')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show(fig_ds)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, to specify which data we want to plot, we passed arrays to the glyph creation methods of our figure (`fig_...`) instances. Where did the data go? (Bear with me for a moment, it is going to get a bit technical.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data is somewhat hidden in the figure objects. Each figure has a `list` of `GlyphRenderer`s which represent the single graphs. In `fig_ds`, there are two scatter plots, i.e. only two `GlyphRenderer`s: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig_ds.renderers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "glyph_renderer = fig_ds.renderers[0]\n",
    "type(glyph_renderer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each `GlyphRenderer` instance has an attribute `data_source`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(glyph_renderer.data_source)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The class that `data_source` belongs to is of type `ColumnDataSource`. Bokeh converts the data we put in internally to `ColumnDataSource` objects and goes on working with the data in this form."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, the reason I stretch your patience with this technicality is that manipulating data sources is a way to make your plots truly interactive, that is, update the data dynamically and see the change live on the screen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`ColumnDataSource` objects have an attribute `data` which is similar to a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "glyph_renderer.data_source.data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that the `data` dictionary has the keys `x` and `y` (these are default name which were set by Bokeh). See what happens if I manipulate the `x` data and redraw the figure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = glyph_renderer.data_source.data['x']\n",
    "x /= 10\n",
    "\n",
    "show(fig_ds)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See how the blue dots are now squeezed together?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### \"Pushing\" changes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next step to dynamically updating Bokeh plots:\n",
    "\n",
    "We have to update the plot without redrawing it. The magical function which triggers the update is called `push_notebook()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bokeh.io import push_notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`push_notebook()` only works together with displayed figures that know that they should be updateable. To flag a figure as updateable, we have to pass the keyword `notebook_handle=True` to the show function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "handle = show(fig_ds, notebook_handle=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this set-up, if we manipulate the data source of `fig_ds` and `push_notebook()`, the plot will be updated without fully re-rendering it (run this cell a couple of times and watch the plot):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "glyph_renderer.data_source.data['x'] = np.random.randn(20)\n",
    "push_notebook(handle=handle)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plotting with a ColumnDataSource"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Digging deep into the figure objects to access the data sources is a bit cumbersome.\n",
    "It is simpler to define data sources before plotting the data and passing the data sources to the plotting functions:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bokeh.plotting import ColumnDataSource\n",
    "\n",
    "fig_ds2 = figure(height=300, width=300, x_range=(-5, 5), y_range=(-5, 5))\n",
    "\n",
    "np.random.seed(42)\n",
    "\n",
    "# pass a dictionary to ColumnDataSource to create\n",
    "# an instance holding your data;\n",
    "# we can choose the key names as we like:\n",
    "data_source = ColumnDataSource(\n",
    "    dict(my_x=np.random.randn(100), my_y=np.random.randn(100))\n",
    ")\n",
    "\n",
    "# use the keyword 'source' to pass the data source when plotting;\n",
    "# use the keywords 'x' and 'y' to define the keys which should\n",
    "# be used as x- and y-data\n",
    "fig_ds2.circle(x='my_x', y='my_y', source=data_source)\n",
    "\n",
    "handle_fig_ds2 = show(fig_ds2, notebook_handle=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can manipulate the data source as before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import sleep\n",
    "\n",
    "for _ in range(20):\n",
    "    data_source.data = dict(my_x=np.random.randn(100),\n",
    "                            my_y=np.random.randn(100))\n",
    "    sleep(0.1)\n",
    "    push_notebook(handle=handle_fig_ds2)  # we only push the figure fig_ds2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: plotting_2.ipynb](plotting_2.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
