{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false
   },
   "source": [
    "# Coding style (I)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2022-10-12\n",
    "\n",
    "N. Luettschwager"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"toc\">*Table of contents*</a>\n",
    "\n",
    "- [Formatting: PEP 8](#Formatting:-PEP-8)\n",
    "- [Code analysis: Linters](#Code-analysis:-Linters)\n",
    "  - [Editor integration](#Editor-integration) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook I want to discuess coding style in a broad sense, i.e. how to format code, how to name things, how to document code and how to structure code. The motivation to follow best practice regarding coding style is simple:\n",
    "You want to write code that you can understand easily in a couple of weeks from now, and you want to be able to reuse that code.\n",
    "\n",
    "[Dedicated environments](environments.ipynb) and [version control](version-control.ipynb), which we already discussed, are (to me) more facets of good coding practice/style."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Formatting: PEP 8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you browse through Python repositories, you will find that their code is formatted mostly using a consistent style. This is because Python has it's own, universal style guide (as other programming languages or projects do). This guide is called [&rarr; PEP 8](https://www.python.org/dev/peps/pep-0008/) (PEP stands for [\"Python Enhancement Proposal\"](glossary.md#pep) 🤓).\n",
    "\n",
    "An example: according to PEP 8, you would write"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "def increment(x, i=1):\n",
    "    return x + i\n",
    "```\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but not:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "def Increment(x,i = 1) :\n",
    "    return x + i\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both variants are vaild Python syntax and vary only in useage of whitespace and capitalization. Still, the latter is regarded as bad style.\n",
    "\n",
    "\"Why bother?\", you may ask. Everybody has their own opinion and sense of style, after all.\n",
    "\n",
    "One reason is that people should be able to work on some code collaboratively, without changing the formatting around and creating hugh *diffs* for no good reason. Another reason is that with time you get used to a specific style and reading strangely formatted code will only increase your mental burden when trying to understand what some code does. So a standard for everybody to follow is in order."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I do not want to re-iterate all the rules that are listed in PEP 8 here, in a bit we will learn how to ask the computer to check our formatting. \n",
    "\n",
    "<a id=\"pep8-examples\"></a>\n",
    "\n",
    "**Let's just look at some of the most common rules:**\n",
    "\n",
    "- For indentation, 4 spaces should be used, not tabs\n",
    "- A line of code should be less than 80 characters\n",
    "- Function and variable names should use only small letters and distinguish words by underscores (\"snake_case\"):\n",
    "\n",
    "> ```python\n",
    "> def function_that_does_x(a, b):\n",
    ">     ...\n",
    "> ```\n",
    " \n",
    "*not*\n",
    " \n",
    "> ```python\n",
    "> def functionThatDoesX(a, b):\n",
    ">     ...\n",
    "> ```\n",
    "\n",
    "and\n",
    " \n",
    "> ```python\n",
    "> my_variable = 'foo'\n",
    "> ```\n",
    "\n",
    "*not*\n",
    " \n",
    "> ```python\n",
    "> myVariable = 'foo'\n",
    "> ```\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Names for classes should use capital letters and \"CamelCase\":\n",
    "\n",
    "> ```python\n",
    "> class MyClass:\n",
    ">     ...\n",
    "> ```\n",
    "\n",
    "*not*\n",
    "\n",
    "> ```python\n",
    "> class my_class:\n",
    ">     ...\n",
    "> ```\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Imports should all appear at the top of the file and should be on separate lines\n",
    "\n",
    "> ```python\n",
    "> import os\n",
    "> import sys\n",
    "> ```\n",
    "\n",
    "*not*\n",
    "\n",
    "> ```python\n",
    "> import os, sys\n",
    "> ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Star (`*`) imports should be avoided\n",
    "\n",
    "> ```python\n",
    "> from scipy.ndimage.filters import median_filter\n",
    "> from scipy.signal import savgol_filter\n",
    ">\n",
    "> ...\n",
    " \n",
    "> filtered_x = savgol_filter(x, 9, 3)\n",
    "> filtered_y = median_filter(y, size=5)\n",
    ">```\n",
    "\n",
    "*not*\n",
    " \n",
    "> ```python\n",
    "> from scipy.ndimage.filters import *\n",
    "> from scipy.signal import *\n",
    ">\n",
    "> ...\n",
    ">\n",
    "> filtered_x = savgol_filter(x, 9, 3)  # were do the filter functions come from??\n",
    "> filtered_y = median_filter(y, size=5)\n",
    "> ```\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Some binary operators should always be surrounded by whitespace:\n",
    "\n",
    "> ```python\n",
    "> a = [1, 2, 3]\n",
    ">```\n",
    "\n",
    "*not*\n",
    " \n",
    "> ```python\n",
    "> a=[1, 2, 3]\n",
    "> ```\n",
    "\n",
    "*and*\n",
    "\n",
    "> ```python\n",
    "> if a > b:\n",
    ">     ...  \n",
    "> ```\n",
    "\n",
    "*not*\n",
    "\n",
    "> ```python\n",
    "> if a>b:\n",
    ">    ...\n",
    "> ```\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- An exception to the above rule are default values in function definitions (keyword arguments):\n",
    "\n",
    "> ```python\n",
    "> def my_function(a, b=None):\n",
    ">     ...\n",
    "> ```\n",
    "\n",
    "*not*\n",
    " \n",
    "> ```python\n",
    "> def my_function(a, b = None):\n",
    ">     ...\n",
    "> ```\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- *Extraneous* whitespace should be avoided:\n",
    "\n",
    "> ```python\n",
    "> a = [1, 2, 3]\n",
    "> ```\n",
    "\n",
    "*not*\n",
    " \n",
    "> ```python\n",
    "> a = [1 , 2 , 3]\n",
    "> ```\n",
    "\n",
    "and\n",
    "\n",
    "> ```python\n",
    "> def my_function(a, b):\n",
    ">    ...\n",
    "> ```\n",
    "\n",
    "*not*\n",
    " \n",
    "> ```python\n",
    "> def my_function(a , b) :\n",
    ">    ...\n",
    "> ```\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How are you supposed to remember all of these rules? The good news is, you don't have to (though you will, with some practice)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false
   },
   "source": [
    "## Code analysis: Linters\n",
    "\n",
    "This is were code checkers, so called [linters](glossary.md#Linter) come in handy. A linter goes through your code and looks for style guide violations, obvious bugs, redundant or unneccessary code, and more. It is a very handy tool that helps you to write better code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For Python there are the popular packages [&rarr; pylint](http://pylint.pycqa.org/en/latest/) and [&rarr; flake8](http://flake8.pycqa.org/en/latest/) to do this job for us.\n",
    "They are already installed in the course's environment. For your own projects you install them via (with the specific environment activated):\n",
    "\n",
    "```bash\n",
    "(env) pip install flake8\n",
    "(env) pip install pep8-naming\n",
    "```\n",
    "\n",
    "or\n",
    "\n",
    "```bash\n",
    "(env) pip install pylint\n",
    "```\n",
    "\n",
    "Afterwards, you can call these linters from the command line using `flake8 <filename>` or `pylint <filename>`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I have included a Python file [`examples/linter_test.py`](examples/linter_test.py) which includes particularly ugly code. Checking this file with `flake8 linter_test.py` shows many warnings (note that we can execute bash from the notebook using the `%%bash` magic function):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash --no-raise-error\n",
    "\n",
    "source env/bin/activate\n",
    "flake8 examples/linter_test.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "As you can see, flake8 lists problems stating the file name, line and column, error code, and verbatim error message. Formatting errors such as those [listed above](#pep8-examples) are reported, but also other helpful hints like unused imports and redundant code are displayed.\n",
    "\n",
    "In the default configuration, pylint is more strict than flake8, and lists more warnings---swap `flake8` in the above cell for `pylint` and run the cell again.\n",
    "\n",
    "What will be reported by flake8 and pylint can be fine-tuned by using global or project specific configuration files, but we will not go into so much detail here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Editor integration\n",
    "\n",
    "Using the linter on the command line may seem cumbersome. Luckily, it is possible to have the linter communicate with your code editor, so you can see errors while you type, similar to the spell checker in Word.\n",
    "\n",
    "In the course, I will demonstrate the VS Code linter functionality and integration of flake8 and pylint.\n",
    "The following video gives an idea of what this looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Video\n",
    "\n",
    "Video(\"figures/vscode-linting.mp4\", width=640, height=480)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Jupyterlab-LSP"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For Jupyter, there exist plug-ins to integrate the linter, most notably [&rarr; jupyterlab-lsp](https://github.com/jupyter-lsp/jupyterlab-lsp), which also includes other features provided by a [\"Language Server\"](https://microsoft.github.io/language-server-protocol/). Note that jupyterlab-lsp must be installed in the environment which runs your Jupyter server.\n",
    "\n",
    "Jupyterlab-lsp is installed when you run the course from mybinder.org. You can activate it by opening a terminal an type:\n",
    "\n",
    "```bash\n",
    "jupyter labextension enable @krassowski/jupyterlab-lsp v3.10.2\n",
    "```\n",
    "\n",
    "Then reload the website (F5) so the Jupyterlab-UI restarts.  \n",
    "Afterwards, you should see that some of the lines in the code cell in this notebook are underlined, and hovering over them shows linter messages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "myVar = np.array([1 , 2 , 3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Black code formatter\n",
    "\n",
    "Manually formatting your code is not necessary when you have a *code formatter* installed.\n",
    "For Python, a common choice is [→ Black](https://black.readthedocs.io/en/stable/) which can be used as a command line tool to format your code. You can try it on my [`examples/linter_test.py`](examples/linter_test.py) file. Run the next code cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash --no-raise-error\n",
    "\n",
    "black examples/linter_test.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can run a `git diff examples/linter_test.py` on the terminal to view the changes.\n",
    "Run the next cell to revert the file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash --no-raise-error\n",
    "\n",
    "git checkout examples/linter_test.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also a Jupyterlab plug-in to automatically reformat the current notebook: [jupyterlab_code_formatter](https://ryantam626.github.io/jupyterlab_code_formatter/).\n",
    "Again, it is installed if you run the course notebooks from mybinder.org. Click *Format notebook*:\n",
    "\n",
    "![format notebook](figures/format-notebook.png)\n",
    "\n",
    "and see what happens with the cell below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "myVar = np.array(\n",
    "    [1 , 2 , 3], dtype = np.int64\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#toc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: style_2.ipynb](style_2.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
