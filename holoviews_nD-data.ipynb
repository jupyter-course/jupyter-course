{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import vega_datasets\n",
    "from holoviews import dim, opts\n",
    "from holoviews.operation.timeseries import rolling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hv.extension(\"bokeh\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# HoloViews: Visualizing Multi-Dimensional Data\n",
    "\n",
    "So far, we have plotted rather simple datasets with only two dimensions.\n",
    "In this notebook, we will learn how we can represent more dimensions in our plots through style mapping and interactivity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Style Mapping"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the following examples, we use the famous Gapminder dataset.\n",
    "The dataset contains information about country and year versus population, life expectancy, and fertility (children per women). Note that `vega_datasets.data.gapminder()` returns a Pandas `DataFrame`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gapminder = vega_datasets.data.gapminder()\n",
    "gapminder.tail(10)  # .tail(n) => show the last n rows"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With a simple scatter plot, we can show that fertility and life expectancy is related:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# you can pass a dataframe directy to a holoviews element and follow that up\n",
    "# with the column for the key dimension and value dimension:\n",
    "hv.Scatter(gapminder, \"life_expect\", \"fertility\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That is already something, but a lot of the interesting information is missing. We cannot see which year each data point belongs to. Also, we cannot see the country or population (the \"weight\" of the data point, so to say). What can we do?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Color\n",
    "\n",
    "A scatter plot has more **visual degrees of freedom** than x and y position that we can leverage. The data points can have a different color, for example. We can *map* the dimension \"year\" onto the *style* \"color\" of the data points by declaring `\"year\"` a value dimension (`vdim`) and then simply choosing `\"year\"` as color in the call to `.opts()`. This is called **style mapping**. Look:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "life_expect_vs_fertility = hv.Scatter(\n",
    "    gapminder, kdims=\"life_expect\", vdims=[\"fertility\", \"year\"]\n",
    ")\n",
    "\n",
    "life_expect_vs_fertility.opts(color=\"year\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great, we can already see that the darker points are in the lower right corner (fewer chilrdren and longer life expectency), so we can assume that dark are the \"later\" years and light the \"earlier\" years. Of course, we do not have to guess, we can (and should) just display a legend:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "life_expect_vs_fertility.opts(colorbar=True, clabel=\"year\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the plot is clearly too narrow, so we should choose a larger `frame_width`. At the same time, it would be nice to have a colormap which differentiates the years a bit better (option `cmap`). You can look up the [&rarr; HoloViews](http://holoviews.org/user_guide/Colormaps.html) documentation to learn about the available colormaps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "life_expect_vs_fertility.opts(cmap=\"coolwarm\", frame_width=400)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It would make sense to indicate the population with the size of each point. Lets try it! Note that the population (`\"pop\"`) is now another value dimension:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "life_expect_vs_fertility = hv.Scatter(\n",
    "    gapminder, kdims=\"life_expect\", vdims=[\"fertility\", \"year\", \"pop\"]\n",
    ")\n",
    "\n",
    "life_expect_vs_fertility.opts(\n",
    "    clabel=\"year\",\n",
    "    color=\"year\",\n",
    "    colorbar=True,\n",
    "    cmap=\"coolwarm\",\n",
    "    frame_width=400,\n",
    "    size=\"pop\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Well, well, something did not work out. What's wrong?\n",
    "\n",
    "Have a look at the dataset again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gapminder.head(3)  # .head(n) => show the first n rows"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The population is on the order of several million (unsuprisingly), and HoloViews does not automatically convert this value into a sensible range for the size of the data points (although it does so for color ...). We have to do this ourselves."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, we could just create a new column in our dataset and put the transformed values there, but there is a better way. We can use the [&rarr; `hv.dim` function](https://holoviews.org/user_guide/Style_Mapping.html#what-are-dim-transforms) (imported in the global namespace in this notebook) to define a mathematical transformation before mapping onto the size attribute. For example, we can just scale the values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "life_expect_vs_fertility.opts(size=dim(\"pop\") * 3e-8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also apply more complex transformations, e.g. normalize to the range \\[0, 1\\], take the square root (to map population to the area of the points, rather than the point radius) and re-scale:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "life_expect_vs_fertility.opts(size=np.sqrt(dim(\"pop\").norm()) * 30)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(Check out the documentation for more on [&rarr; \"dim-transforms\"](https://holoviews.org/user_guide/Style_Mapping.html#what-are-dim-transforms).)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the size range is more compressed, which is good to see the smaller countries. We could now go on to map countries (or regions) to markers, to marker rotation angles, or alpha (transparency), but that would make the plot too complicated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a last refinement (for now), lets activate the hover tool (`tools` option) and tweak the transparency (`alpha`), so we can see overlapping points:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "life_expect_vs_fertility.opts(tools=[\"hover\"], alpha=0.5, line_alpha=0, hover_alpha=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may notice that the hover tool does not show the country. This is because we have not declared it as a value dimension. Here is how to make the plot from scratch, including showing the country when hovering:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "life_expect_vs_fertility = hv.Scatter(\n",
    "    gapminder, kdims=\"life_expect\", vdims=[\"fertility\", \"year\", \"pop\", \"country\"]\n",
    ")\n",
    "\n",
    "life_expect_vs_fertility.opts(\n",
    "    alpha=0.5,\n",
    "    clabel=\"year\",\n",
    "    color=\"year\",\n",
    "    colorbar=True,\n",
    "    cmap=\"coolwarm\",\n",
    "    frame_width=600,\n",
    "    frame_height=400,\n",
    "    fontsize=12,\n",
    "    hover_alpha=1,\n",
    "    line_alpha=0,\n",
    "    size=np.sqrt(dim(\"pop\").norm()) * 30,\n",
    "    tools=[\"hover\"],\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 1\n",
    "\n",
    "Use the `vega_datasets.data.cars()` dataset and prepare a scatterplot of `Miles_per_Gallon` versus `Horsepower` where the color encodes `Weight_in_lbs` and the marker size the number of `Cylinders`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `HoloMap`s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the gapminder example, we squeezed much additional information into the plot by using marker color and size.\n",
    "Another strategy is to \"hide\" certain dimensions, and make specific values selectable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the example of plotting a sine curve with different fequencies.\n",
    "We could use color plus a legend and make an overlay to plot the extra dimension \"frequency\", but with many different frequencies, the plot would soon get too crowded.\n",
    "\n",
    "What we can do instead is to show one plot at a time and make the frequency selectable.\n",
    "\n",
    "To accomplish this, we can pass a dictionary for different frequencies holding Curves to a `hv.HoloMap`. Such a dictionary is conveniently defined using a *dict-comprehension*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = np.linspace(0, 2 * np.pi, 100)\n",
    "sine_curves = {  # a dict-comprehension: {key: value(x) for x in iterable}\n",
    "    f: hv.Curve((t, np.sin(f * t)), \"time\", \"signal\") for f in np.arange(1, 10, 0.5)\n",
    "}\n",
    "\n",
    "sine_curves"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The key dimension of the HoloMap is the same as the key we used for the dictionary, i.e. the frequency."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hv.HoloMap(sine_curves, kdims=hv.Dimension(\"frequency\", unit=\"Hz\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, we are presented with a plot and a slider widget that lets us control the frequency. In this way we can visualize the extra dimension, one frequency at a time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are two examples that make a HoloMap out of our gapminder plot. As you can see, aside from the different styling (call to `opts()`), not much is required to generate an interactive plot from what we already had."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scatter = life_expect_vs_fertility.clone()  # make a copy of original plot\n",
    "\n",
    "scatter.opts(\n",
    "    alpha=1,\n",
    "    clabel=\"pop\",\n",
    "    cmap=\"reds\",\n",
    "    color=dim(\"pop\").log10(),  # logarithmic population scale\n",
    "    frame_width=400,\n",
    "    frame_height=300,\n",
    "    hover_alpha=0.5,\n",
    "    size=15,\n",
    ")\n",
    "\n",
    "hv.HoloMap(\n",
    "    {y: scatter.select(year=y) for y in scatter.data[\"year\"].unique()}, kdims=\"year\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scatter = life_expect_vs_fertility.clone()\n",
    "\n",
    "scatter.opts(\n",
    "    alpha=1,\n",
    "    clabel=\"year\",\n",
    "    cmap=\"spectral\",\n",
    "    color=\"year\",\n",
    "    frame_width=400,\n",
    "    frame_height=300,\n",
    "    hover_alpha=0.5,\n",
    "    size=15,\n",
    ")\n",
    "\n",
    "hv.HoloMap(\n",
    "    {c: scatter.select(country=c) for c in scatter.data[\"country\"].unique()},\n",
    "    kdims=\"Country\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The later example is supposed to show two things: 1. If the key dimension is categorical data, HoloViews automatically chooses a drop down menu as interaction widget. 2. If you execute the code cell, you may notice that it takes quite some time for the plot to appear. This is because a HoloMap works by generating the plots for _all possible input values_ (here all countries) and sends this to the browser. The benefit is that a HoloMap can be stored as HTML and be used _without executing Python or HoloViews in the background_. The drawback is that, depending on how large the parameter space is, the resulting HTML and JavaScript can become rather large, and generating it takes time.\n",
    "\n",
    "This is were `DynamicMap`s come into play."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dynamic Maps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppose you would like to visualize the sine curve for different frequencies _and_ phase values. Fine control of frequency and phase could easily lead to way too many combinations and thus plots for a HoloMap.\n",
    "\n",
    "To get an interactive figure non the less, a `DynamicMap` creates the requested figure on the fly. To do this, it sends a request from the browser to the Python kernel that is run by Jupyter. The Python kernel executes a specific function (provided by us) that generates a HoloViews element for each particular combination of input parameters and sends it back to the browser. This is much faster than generating all possible combinations as in case of a `HoloMap`. However, a `DynamicMap` cannot just be stored to HTML, it needs a Python kernel that runs in the background and reacts to the user input.\n",
    "\n",
    "The first ingredient for a `DynamicMap` is a function that generates a HoloViews element:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gen_sine(f, p):  # generate a sine curve from frequency and phase\n",
    "    t = np.linspace(0, 2 * np.pi, 100)\n",
    "    return hv.Curve((t, np.sin(f * t + p), \"time\", \"signal\"))\n",
    "\n",
    "\n",
    "gen_sine(1, 0) * gen_sine(2, np.pi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This function is passed as the first argument to `hv.DynamicMap`. In contrast to a `HoloMap` where we explicitly generate HoloViews elements for specific input values, a `DynamicMap` has no way to know what values or ranges should be allowed as parameter space, so we have to declare them using `hv.Dimension`s:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hv.DynamicMap(\n",
    "    gen_sine,\n",
    "    kdims=[\n",
    "        hv.Dimension(\"frequency\", values=np.arange(0, 10, 0.1)),\n",
    "        hv.Dimension(\"phase\", range=(0, 2 * np.pi)),\n",
    "    ],\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have a lot of freedom when defining the generating function, so we can easily create more complex dashboards.\n",
    "Here are two more examples using the gapminder plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gen_gapminder(country):\n",
    "    scatter = life_expect_vs_fertility.clone()\n",
    "    selected = scatter.select(country=country)\n",
    "\n",
    "    scatter.opts(alpha=0.2, frame_width=400, frame_height=300)\n",
    "    selected.opts(alpha=1, line_alpha=1, line_color=\"magenta\", line_width=1)\n",
    "\n",
    "    return scatter * selected\n",
    "\n",
    "\n",
    "hv.DynamicMap(\n",
    "    gen_gapminder, kdims=hv.Dimension(\"Country\", values=gapminder[\"country\"].unique())\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gen_gapminder(country):\n",
    "    all_countries = life_expect_vs_fertility.clone()\n",
    "    selected_country = all_countries.select(country=country)\n",
    "    pop_vs_year = hv.Curve(gapminder.query(\"country == @country\"), \"year\", \"pop\")\n",
    "\n",
    "    pop_vs_year = pop_vs_year.transform((\"pop\", dim(\"pop\") / 1e6))\n",
    "    pop_vs_year = pop_vs_year.redim(pop=hv.Dimension(\"population\", unit=\"10⁶\"))\n",
    "\n",
    "    all_countries.opts(alpha=0.2, clabel=\"year\", frame_width=400, frame_height=300)\n",
    "    selected_country.opts(alpha=1, line_alpha=1, line_color=\"magenta\", line_width=1)\n",
    "    pop_vs_year.opts(\n",
    "        framewise=True, frame_width=400, frame_height=200, color=\"blue\", fontsize=12\n",
    "    )\n",
    "\n",
    "    layout = all_countries * selected_country + pop_vs_year\n",
    "\n",
    "    return layout.cols(1)\n",
    "\n",
    "\n",
    "hv.DynamicMap(\n",
    "    gen_gapminder,\n",
    "    kdims=hv.Dimension(\n",
    "        \"Country\", values=gapminder[\"country\"].unique(), default=\"Germany\"\n",
    "    ),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise 2\n",
    "\n",
    "Adapt your plot from Exercise 1 to\n",
    "1. ... create a `HoloMap` where the `Origin` of the car (\"USA\", \"Europe\" or \"Japan\") is selectable.\n",
    "2. ... create a `DynamicMap` where the `Year` is selectable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Streams"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the dynamic maps above, we used input widgets (dropdowns, sliders) that were automatically generated by HoloViews.\n",
    "\n",
    "It is also possible to get the input for a generating function directly from a plot, for example from selections or the mouse position.\n",
    "The way to make this happen is via so-called *streams*.\n",
    "\n",
    "Here is a simple example (click the points in the plot and see what happens):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scatter = hv.Scatter(([1, 2, 3, 4], [1, 2, 3, 4]))\n",
    "scatter.opts(tools=[\"tap\"], size=20)\n",
    "\n",
    "selection = hv.streams.Selection1D(source=scatter)\n",
    "\n",
    "\n",
    "def upon_selection(index):\n",
    "    return hv.Text(2.5, 2.5, f\"selected index: {index}\")\n",
    "\n",
    "\n",
    "hv.DynamicMap(upon_selection, streams=[selection]) * scatter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's go through this example step by step: 1. We create a simple scatter plot and activate the \"tap\" tool, which allows us to select a single data point. 2. We create a `Selection1D` streams object and tell it, that its source is the scatter plot. Therefore, it will \"listen\" to the selection in the scatter plot and store the index of the selected point. 3. We define a function that generates a HoloViews element from an index. Here, I choose just a simple text label. 4. We declare the `DynamicMap` with the generating function and the `Selection1D` streams object. The latter is passed to `DynamicMap` via the keyword argument `streams` (note that streams are always passed as a list, since there can be multiple streams). Finally, we overlay the dynamic map with the scatter plot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the same scheme to select a country in the gapminder example, just by selecting a point rather than choosing a country from a dropdown menu. Much more natural and user friendly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "all_countries = life_expect_vs_fertility.clone()\n",
    "\n",
    "all_countries.opts(\n",
    "    alpha=0.2, clabel=\"year\", frame_width=400, frame_height=300, tools=[\"tap\", \"hover\"]\n",
    ")\n",
    "\n",
    "selection = hv.streams.Selection1D(source=all_countries)\n",
    "\n",
    "\n",
    "def gen_gapminder(index):\n",
    "    if index:\n",
    "        c = all_countries.data.loc[index[0], \"country\"]\n",
    "    else:\n",
    "        c = \"Germany\"\n",
    "    selected_country = all_countries.select(country=c)\n",
    "    selected_country.opts(\n",
    "        alpha=1, line_alpha=1, line_color=\"magenta\", line_width=1, title=c\n",
    "    )\n",
    "    return selected_country\n",
    "\n",
    "\n",
    "hv.DynamicMap(gen_gapminder, streams=[selection]) * all_countries"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a somewhat more complex interaction example using the mouse position (`hv.streams.PointerXY`) in an STM image of a graphite surface:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create holoviews image from xyz file\n",
    "\n",
    "df = pd.read_csv(\"data/STM_graphite.xyz\", delimiter=\"\\t\", names=[\"x\", \"y\", \"z\"]).pivot(\n",
    "    index=\"x\", columns=\"y\", values=\"z\"\n",
    ")\n",
    "y = df.columns.values * 1e9\n",
    "x = df.index.values * 1e9\n",
    "Z = df.values * 1e9\n",
    "\n",
    "stm_dim = lambda d: hv.Dimension(d, unit=\"nm\")\n",
    "STM_graphite_image = hv.Image(\n",
    "    (x, y, Z), kdims=[stm_dim(\"x\"), stm_dim(\"y\")], vdims=stm_dim(\"z\")\n",
    ")\n",
    "STM_graphite_image.opts(aspect=\"equal\")\n",
    "\n",
    "# create dynamic map\n",
    "select = hv.streams.PointerXY(source=STM_graphite_image)\n",
    "\n",
    "\n",
    "def stm_cuts(x=0, y=0):\n",
    "    x = 0 if y is None else x\n",
    "    y = 0 if y is None else y\n",
    "\n",
    "    x_curve = STM_graphite_image.sample(y=y, closest=True).opts(title=f\"x = {x:.2f}\")\n",
    "    y_curve = STM_graphite_image.sample(x=x, closest=True).opts(title=f\"y = {y:.2f}\")\n",
    "\n",
    "    x_smooth = rolling(x_curve).relabel(label=\"smoothed\")\n",
    "    y_smooth = rolling(y_curve).relabel(label=\"smoothed\")\n",
    "\n",
    "    return x_curve * x_smooth + y_curve * y_smooth\n",
    "\n",
    "\n",
    "STM_graphite_image + hv.DynamicMap(stm_cuts, streams=[select]).collate()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also the possibility to add a *callback* function to a stream, by declaring the function a \"subscriber\".\n",
    "The callback function is than executed each time the value of the stream changes. In that way, you can make HoloViews \"talk\" to other interactive elements of the Jupyter ecosystem, for example, to widgets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets.widgets import Text\n",
    "\n",
    "img = hv.Image(np.random.randn(100, 100)).opts(tools=[\"tap\"])\n",
    "select = hv.streams.PointerXY(source=img)\n",
    "\n",
    "t = Text(description=\"position:\")\n",
    "\n",
    "\n",
    "def update_t(x=0, y=0):\n",
    "    t.value = f\"{x:.2f}, {y:.2f}\"\n",
    "\n",
    "\n",
    "select.add_subscriber(update_t)\n",
    "\n",
    "display(t)\n",
    "img"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This concludes our introduction of HoloViews. If you want to learn more, check out the HoloViews [&rarr; getting started](http://holoviews.org/getting_started/index.html) guide and the other [&rarr; user guides](http://holoviews.org/user_guide/index.html).\n",
    "\n",
    "There are quite some things left to explore, for example how to manipulate data with HoloViews [&rarr; transformations and operations](https://holoviews.org/user_guide/Transforming_Elements.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solutions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cars = vega_datasets.data.cars()\n",
    "\n",
    "# Miles_per_Gallon versus Horsepower\n",
    "# color ⇒ Weight_in_lbs\n",
    "#marker size ⇒ Cylinders\n",
    "hv.Scatter(\n",
    "    cars,\n",
    "    kdims=\"Horsepower\",\n",
    "    vdims=[\"Miles_per_Gallon\", \"Name\", \"Weight_in_lbs\", \"Cylinders\"],\n",
    ").opts(\n",
    "    color=\"Weight_in_lbs\",\n",
    "    colorbar=True,\n",
    "    cmap=\"spectral\",\n",
    "    clabel=\"Weight in lbs\",\n",
    "    frame_width=400,\n",
    "    size=\"Cylinders\",\n",
    "    tools=[\"hover\"],\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1: Origin selectable with HoloMap\n",
    "cars = vega_datasets.data.cars()\n",
    "\n",
    "cars_plot = hv.Scatter(\n",
    "    cars,\n",
    "    kdims=\"Horsepower\",\n",
    "    vdims=[\"Miles_per_Gallon\", \"Name\", \"Weight_in_lbs\", \"Cylinders\", \"Origin\"],\n",
    ")\n",
    "cars_plot.opts(\n",
    "    color=\"Weight_in_lbs\",\n",
    "    colorbar=True,\n",
    "    cmap=\"spectral\",\n",
    "    clabel=\"Weight in lbs\",\n",
    "    frame_width=400,\n",
    "    size=\"Cylinders\",\n",
    "    tools=[\"hover\"],\n",
    ")\n",
    "\n",
    "hv.HoloMap(\n",
    "    {origin: cars_plot.select(Origin=origin) for origin in cars.Origin.unique()},\n",
    "    kdims=\"Origin\",\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 2: Year selectable with DynamicMap\n",
    "cars = vega_datasets.data.cars()\n",
    "\n",
    "def make_cars_plot(year):\n",
    "    plot = hv.Scatter(\n",
    "        cars[cars[\"Year\"] == year],\n",
    "        kdims=\"Horsepower\",\n",
    "        vdims=[\"Miles_per_Gallon\", \"Name\", \"Weight_in_lbs\", \"Cylinders\"],\n",
    "    )\n",
    "    plot.opts(\n",
    "        color=\"Weight_in_lbs\",\n",
    "        colorbar=True,\n",
    "        cmap=\"spectral\",\n",
    "        clabel=\"Weight in lbs\",\n",
    "        frame_width=400,\n",
    "        size=\"Cylinders\",\n",
    "        tools=[\"hover\"],\n",
    "    )\n",
    "    return plot\n",
    "\n",
    "\n",
    "hv.DynamicMap(make_cars_plot, kdims=hv.Dimension(\"year\", values=cars.Year.unique()))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
